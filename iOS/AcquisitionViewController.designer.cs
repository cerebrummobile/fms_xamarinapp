// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("AcquisitionViewController")]
    partial class AcquisitionViewController
    {
        [Outlet]
        UIKit.UIImageView play_pause_img { get; set; }


        [Outlet]
        UIKit.UIButton play_pauseBtn { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}