﻿using Foundation;
using UIKit;

namespace IAO
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations

		public override UIWindow Window
		{
			get;
			set;
		}
		UIStoryboard  storyboard = UIStoryboard.FromName("Main", null);

		public BaseNavigationController NavController { get; private set; }
		public static UINavigationController NavigationController;
		public RootViewController RootViewController { get { return Window.RootViewController as RootViewController; } }

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{     

			System.Threading.Thread.Sleep(4000);
			//var storyBoard_ = UIStoryboard.FromName("Main", null);
			//var initialViewController = storyBoard_.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;

			//var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			//if (device_type.ToString().Equals("Pad"))
			//{
			//	var content = storyboard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
			//	NavigationController = new UINavigationController(content);
			//}
			//else
			//{
			//	var content_ = storyboard.InstantiateViewController("HomeController") as HomeViewController;

			//	NavigationController = new UINavigationController(content_);
			//}
		//	NavigationController.NavigationBar.BackgroundColor = UIColor.FromRGB(206, 211, 217);
		//	NavigationController.NavigationBar.Translucent = false;
			////NavController = new BaseNavigationController(content_);
			////NavController.PresentViewController(content_, false, null);
			//Window.RootViewController = NavigationController;
			// Override point for customization after application launch.
			// If not required for your application you can safely delete this method
			//var window = new UIWindow(UIScreen.MainScreen.Bounds);
			//Window.RootViewController = new RootViewController();
			//Window.MakeKeyAndVisible();

		return true;
		}

		public override void OnResignActivation(UIApplication application)
		{
			// Invoked when the application is about to move from active to inactive state.
			// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
			// or when the user quits the application and it begins the transition to the background state.
			// Games should use this method to pause the game.
		}

		public override void DidEnterBackground(UIApplication application)
		{
			// Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
		}

		public override void WillEnterForeground(UIApplication application)
		{
			// Called as part of the transiton from background to active state.
			// Here you can undo many of the changes made on entering the background.
		}

		public override void OnActivated(UIApplication application)
		{
			// Restart any tasks that were paused (or not yet started) while the application was inactive. 
			// If the application was previously in the background, optionally refresh the user interface.
		}

		public override void WillTerminate(UIApplication application)
		{
			Settings.teeth_nameSettings = "0";
			// Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
		}




	}
}

