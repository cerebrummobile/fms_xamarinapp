using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace IAO
{
	public partial class HelpImageViewScreen : BaseController
    {
		UIImageView imageview;
		public string firstImage = "";
		public string secondImage = "";
	
        public HelpImageViewScreen (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			View.BackgroundColor = UIColor.FromRGB(0, 13, 29);

			RightBaritemClickListner();

			LayoutCreation();
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			if (device_type.ToString().Equals("Pad"))
			{
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 23f), ForegroundColor = UIColor.Black };

			}
			else
			{
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 18f), ForegroundColor = UIColor.Black };


			}
		}

		void LayoutCreation()
		{
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			imageview = new UIImageView();
			if (firstImage.Equals("Image1"))
			{
				imageview.Image = UIImage.FromFile("help_IMG1");
				this.NavigationItem.Title = "Help (IMAGE1)".ToUpper();
			}
			else if (secondImage.Equals("Image2"))
			{ 
				imageview.Image = UIImage.FromFile("help_IMG2");
				this.NavigationItem.Title = "Help (IMAGE2)";
			}

			if (View.Frame.Width == 736 && View.Frame.Height == 414)
			{//6S PLUS
				imageview.Frame = new CGRect(0, 20, View.Frame.Width , View.Frame.Height - 20);
			}

			if (View.Frame.Width == 667 && View.Frame.Height == 375)
			{//6S
				imageview.Frame = new CGRect(0, 10, View.Frame.Width , View.Frame.Height - 10);
			}

			if (View.Frame.Width == 568 && View.Frame.Height == 320)
			{//5S
				imageview.Frame = new CGRect(0, 10, View.Frame.Width, View.Frame.Height - 10);
			}

			if (View.Frame.Width == 480 && View.Frame.Height == 320)
			{//4s same as 5s
				imageview.Frame = new CGRect(0, 10, View.Frame.Width, View.Frame.Height - 20);
			}

			if (device_type.ToString().Equals("Pad"))
			{
				imageview.Frame = new CGRect(0, 10, View.Frame.Width, View.Frame.Height- 20);
 			}

			View.AddSubviews(imageview);
		}

		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new CGRect(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
					HomeScreenControllerIPaid.buttonclicktag = "";
				HomeViewController.ButtonClickTag = "";
				var storyBoard_ = UIStoryboard.FromName("Main", null);
				var initialViewController = storyBoard_.InstantiateViewController("root");
				UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}
    }
}