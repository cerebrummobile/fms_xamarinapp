/* This class is playing the video of the teeth 
 *  */
using Foundation;
using System;
using UIKit;
using AVFoundation;
using AVKit;
using CoreGraphics;
using System.Drawing;

namespace IAO
{
	public partial class AcquisitionViewController : BaseController
    {    AVPlayer avp;
		AVPlayerLayer _playerLayer;
		AVAsset _asset;
		AVPlayerItem _playerItem;

        public AcquisitionViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			//Settings.teeth_nameSettings = "0";
			//this.View.BackgroundColor = UIColor.FromRGB(25, 25, 25);

			var url = NSUrl.FromFilename(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_video);
		  avp = new AVPlayer(url);
		  var avpvc = new AVPlayerViewController();

			avpvc.Player = avp;
			AddChildViewController(avpvc);
			View.AddSubview(avpvc.View);
			avpvc.View.Frame	= new CGRect(0, 0 + 40 , View.Frame.Size.Width , View.Frame.Size.Height - 44);
			avpvc.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			avpvc.View.Layer.CornerRadius = 30;
			avpvc.ShowsPlaybackControls = true;
			avp.Play();
			this.NavigationItem.Title = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name +"(Acquisition)".ToUpper();
			/// set the right side app icon with clcik listner
			RightBaritemClickListner();
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			avp.Pause();
		}
		/// <summary>
		/// set the right side app icon with clcik listner
		/// </summary>
		protected void RightBaritemClickListner()
		{
			 var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);
				if (device_type.ToString().Equals("Pad"))
				{
					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}

				else
				{
					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}

    }
}