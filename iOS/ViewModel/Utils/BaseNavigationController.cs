﻿using System.Drawing;
using UIKit;

namespace IAO

{
	public class BaseNavigationController : UINavigationController
	{
		public BaseNavigationController ()
		{
			Initialize ();
		}

		public BaseNavigationController (UIViewController rootViewController) : base (rootViewController)
		{
			Initialize ();
		}

		void Initialize ()
		{    
			NavigationBar.BackgroundColor = UIColor.FromRGB(206, 211, 217);

			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			if (device_type.ToString().Equals("Pad"))
			{
				NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 23f), ForegroundColor = UIColor.Black };

			}
			else
			{
				NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 18f), ForegroundColor = UIColor.Black };


			}

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();



		}

		public override bool PrefersStatusBarHidden ()
		{
			return false;
		}
	}
}

