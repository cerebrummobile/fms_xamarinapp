﻿/* This class contains all the uiviewcontroller method and set the humbericon with menu opetions 
 * and data of 18 images to the list
 *  */
using System;
using System.Drawing;
using SidebarNavigation;

using Foundation;
using UIKit;
using System.Collections.Generic;

namespace  IAO
{
	public  class BaseController : UIViewController
	{    
		public static List<TeethsInfo> teeth_datalist;
		public static bool Menu_btn_hideShow = false;
		string buttonTagg = "";
		// provide access to the sidebar controller to all inheriting controllers
		public SidebarNavigation.SidebarController sidebarController
		{
			get
			{
				return (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.sideBar;
			}
		}



		//// provide access to the sidebar controller to all inheriting controllers
		public BaseNavigationController NavController { 
			get {
				return (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.NavController;
			} 
		}

		public BaseController(string nibName, NSBundle bundle) : base(nibName, bundle)
		{
		}
		public BaseController(IntPtr handle) : base(handle)
		{
		}
		public BaseController()
		{
		}

		public static List<TeethsInfo> GetTeethsDataList
		{
			get
			{
				return setDataInList();
			}
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//AppDelegate.NavigationController.NavigationItem.
			// info_Button

			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			if (device_type.ToString().Equals("Pad"))
			{
				buttonTagg = HomeScreenControllerIPaid.buttonclicktag;
			}
			else
			{
				buttonTagg =  HomeViewController.ButtonClickTag;

			}

			if (!(buttonTagg.Equals("Help_Button") || buttonTagg.Equals("info_Button")))
			{
				NavigationItem.SetLeftBarButtonItem(
				new UIBarButtonItem(UIImage.FromBundle("hamburger").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
							, UIBarButtonItemStyle.Plain
							, (sender, args) =>
							{  // sidebarController_
								sidebarController.ToggleMenu();
							}), true);
			}
			else
			{
				
			}


		}



		public	static	List<TeethsInfo> setDataInList()
		{
			teeth_datalist = new List<TeethsInfo>() {
				new TeethsInfo(){
					teeth_id="0",
					teeth_name="Maxillary Molar Periapical",
					teeth_img ="PA1",
					teeth_video = "Video/Video6.mp4",
					teeth_anatomy = "PA1/anatomy",
					teeth_error1 = "PA1/error1",
					teeth_error2 = "PA1/error2",
					teeth_cretiria = "PA1/criteria",
					 teeth_landing1 = "PA1/ideal1",
					teeth_landing2 = "PA1/ideal2",

				},
				new TeethsInfo(){
					teeth_id="1",
					teeth_name="Maxillary Premolar Periapical",
					teeth_img ="PA2",
					teeth_video = "Video/Video6.mp4",
					teeth_anatomy = "PA2/anatomy",
					teeth_error1 = "PA2/error1",
					teeth_error2 = "PA2/error2",
					teeth_cretiria = "PA2/criteria",
					 teeth_landing1 = "PA2/ideal1",
					teeth_landing2 = "PA2/ideal2",
				},
				new TeethsInfo(){
					teeth_id="2",
					teeth_name="Maxillary Canine Periapical",
					teeth_img = "PA3",
					teeth_video = "Video/Video3.mp4",
										teeth_error2 = string.Empty,

					teeth_anatomy = "PA3/anatomy",
					teeth_error1 = "PA3/error1",
					teeth_cretiria = "PA3/criteria",
					 teeth_landing1 = "PA3/ideal1",
					teeth_landing2 = "PA3/ideal2",
				},
				new TeethsInfo(){
					teeth_id="3",
					teeth_name="Maxillary Central Incisor Periapical",
					teeth_img ="PA4",
					teeth_video = "Video/Video4.mp4",
					teeth_anatomy = "PA4/anatomy",
					teeth_error1 = "PA4/error1",
					teeth_error2 = "PA4/error2",
					teeth_cretiria = "PA4/criteria",
					 teeth_landing1 = "PA4/ideal1",
					teeth_landing2 = "PA4/ideal2",
				},
				new TeethsInfo(){
					teeth_id="4",
					teeth_name="Maxillary Canine Periapical",
					teeth_img ="PA5",
					teeth_video = "Video/Video6.mp4",
					teeth_error2 = string.Empty,
					teeth_anatomy = "PA5/anatomy",
					teeth_error1 = "PA5/error1",
					teeth_cretiria = "PA5/criteria",
					 teeth_landing1 = "PA5/ideal1",
					teeth_landing2 = "PA5/ideal2",
				},
				new TeethsInfo(){
					teeth_id="5",
					teeth_name="Maxillary Premolar Periapical",
					teeth_img ="PA6",
					teeth_video = "Video/Video6.mp4",
					teeth_anatomy = "PA6/anatomy",
					teeth_error1 = "PA6/error1",
					teeth_error2 = "PA6/error2",
					teeth_cretiria = "PA6/criteria",
					 teeth_landing1 = "PA6/ideal1",
					teeth_landing2 = "PA6/ideal2",
				},
				new TeethsInfo(){
					teeth_id="6",
					teeth_name="Maxillary Molar Periapical",
					teeth_img ="PA7",
					teeth_video = "Video/Video6.mp4",
					teeth_anatomy = "PA7/anatomy",
					teeth_error1 = "PA7/error1",
					teeth_error2 = "PA7/error2",
					teeth_cretiria = "PA7/criteria",
					 teeth_landing1 = "PA7/ideal1",
					teeth_landing2 = "PA7/ideal2",
				},
				new TeethsInfo(){
					teeth_id="7",
					teeth_name="Mandibular Molar Periapical",
					teeth_img ="PA8",
					teeth_video = "Video/Video5.mp4",
										teeth_error2 = string.Empty,
					teeth_anatomy = "PA8/anatomy",
					teeth_error1 = "PA8/error1",
					teeth_cretiria = "PA8/criteria",
					 teeth_landing1 = "PA8/ideal1",
					teeth_landing2 = "PA8/ideal2",
				},
				new TeethsInfo(){
					teeth_id="8",
					teeth_name="Mandibular Premolar Periapical",
					teeth_img ="PA9",
					teeth_video = "Video/Video5.mp4",

										teeth_error2 = string.Empty,
				teeth_anatomy = "PA9/anatomy",
					teeth_error1 = "PA9/error1",
					teeth_cretiria = "PA9/criteria",
					 teeth_landing1 = "PA9/ideal1",
					teeth_landing2 = "PA9/ideal2",
				},
				new TeethsInfo(){
					teeth_id="9",
					teeth_name="Mandibular Canine Periapical",
					teeth_img ="PA10",
					teeth_video = "Video/Video1.mp4",
					teeth_anatomy = "PA10/anatomy",
					teeth_error1 = "PA10/error1",
					teeth_error2 = "PA10/error2",
					teeth_cretiria = "PA10/criteria",
					 teeth_landing1 = "PA10/ideal1",
					teeth_landing2 = "PA10/ideal2",
				},
				new TeethsInfo(){
					teeth_id="10",
					teeth_name="Mandibular Central Incisor Periapical",
					teeth_img ="PA11",
					teeth_video = "Video/Video2.mp4",
					teeth_error2 = string.Empty,
					teeth_landing2 = string.Empty,
					teeth_anatomy = "PA11/anatomy",
					teeth_error1 = "PA11/error1",
					teeth_cretiria = "PA11/criteria",
					 teeth_landing1 = "PA11/ideal1",
				},
				new TeethsInfo(){
					teeth_id="11",
					teeth_name="Mandibular Canine Periapical",
					teeth_img ="PA12",
					teeth_video = "Video/Video1.mp4",
					teeth_anatomy = "PA12/anatomy",
					teeth_error1 = "PA12/error1",
					teeth_error2 = "PA12/error2",
					teeth_cretiria = "PA12/criteria",
					 teeth_landing1 = "PA12/ideal1",
					teeth_landing2 = "PA12/ideal2",
				},
				new TeethsInfo(){
					teeth_id="12",
					teeth_name="Mandibular Premolar Periapical",
					teeth_img ="PA13",
					teeth_video = "Video/Video5.mp4",
					teeth_error2 = string.Empty,
					teeth_anatomy = "PA13/anatomy",
					teeth_error1 = "PA13/error1",
					teeth_cretiria = "PA13/criteria",
					 teeth_landing1 = "PA13/ideal1",
					teeth_landing2 = "PA13/ideal2",
				},
				new TeethsInfo(){
					teeth_id="13",
					teeth_name="Mandibular Molar Periapical",
					teeth_img ="PA14",
					teeth_video = "Video/Video5.mp4",
					teeth_error2 = string.Empty,
					teeth_anatomy = "PA14/anatomy",
					teeth_error1 = "PA14/error1",
					teeth_cretiria = "PA14/criteria",
					 teeth_landing1 = "PA14/ideal1",
					teeth_landing2 = "PA14/ideal2",
				},
				new TeethsInfo(){
					teeth_id="14",
					teeth_name="Molar Bitewing",
					teeth_img ="BW1",
					teeth_video = "Video/Video7.mp4",
					teeth_anatomy = "BW1/anatomy",
					teeth_error1 = "BW1/error1",
					teeth_error2 = "BW1/error2",
					teeth_cretiria = "BW1/criteria",
					 teeth_landing1 = "BW1/ideal1",
					teeth_landing2 = "BW1/ideal2",
				},
				new TeethsInfo(){
					teeth_id="15",
					teeth_name="Premolar Bitewing",
					teeth_img ="BW2",
					teeth_video = "Video/Video7.mp4",

					teeth_error2 = string.Empty,
					teeth_anatomy = "BW2/anatomy",
					teeth_error1 = "BW2/error1",
					teeth_cretiria = "BW2/criteria",
					 teeth_landing1 = "BW2/ideal1",
					teeth_landing2 = "BW2/ideal2",
				},
				new TeethsInfo(){
					teeth_id="16",
					teeth_name="Premolar Bitewing",
					teeth_img ="BW3",
					teeth_video = "Video/Video7.mp4",

					teeth_error2 = string.Empty,
					teeth_anatomy = "BW3/anatomy",
					teeth_error1 = "BW3/error1",
					teeth_cretiria = "BW3/criteria",
					 teeth_landing1 = "BW3/ideal1",
					teeth_landing2 = "BW3/ideal2",
				},
				new TeethsInfo(){
					teeth_id="17",
					teeth_name="Molar Bitewing",
					teeth_img ="BW4",
					teeth_video = "Video/Video7.mp4",
					teeth_anatomy = "BW4/anatomy",
					teeth_error1 = "BW4/error1",
					teeth_error2 = "BW4/error2",
					teeth_cretiria = "BW4/criteria",
					 teeth_landing1 = "BW4/ideal1",
					teeth_landing2 = "BW4/ideal2",
				},

			};
			return teeth_datalist;
		}


	}
}

