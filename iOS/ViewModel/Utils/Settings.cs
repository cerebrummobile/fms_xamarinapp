﻿using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;

namespace IAO
{
	public static class Settings
	{
		private static ISettings AppSettings {
			get {
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string video_selected_key = "video_selected_key";
		private static readonly string video_selected_key_default = string.Empty;

		private const string teethAnatomy_key = "teethAnatomy_key";
		private static readonly string teethAnatomy_key_default = string.Empty;

		private const string teeth_cretiria_key = "teeth_cretiria_key";
		private static readonly string teeth_cretiria_key_default = string.Empty;


		private const string teeth_error1_key = "teeth_error1_key";
		private static readonly string teeth_error1_key_default = string.Empty;

		private const string teeth_error2_key = "teeth_error2_key";
		private static readonly string teeth_error2_key_default = string.Empty;

		private const string teeth_nameKey = "teeth_name";
		private static readonly string teeth_name_key_default = "0";
		#endregion
		public static string video_selected_keySettings {
			get {
				return AppSettings.GetValueOrDefault (video_selected_key, video_selected_key_default);
			}
			set {
				AppSettings.AddOrUpdateValue (video_selected_key, value);
			}
		}

		public static string teeth_nameSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(teeth_nameKey, teeth_name_key_default);
			}
			set
			{
				AppSettings.AddOrUpdateValue(teeth_nameKey, value);
			}
		}

		public static string teethAnatomySettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(teethAnatomy_key, teethAnatomy_key_default);
			}
			set
			{
				AppSettings.AddOrUpdateValue(teethAnatomy_key, value);
			}
		}

		public static string teethCretiriaSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(teeth_cretiria_key, teeth_cretiria_key_default);
			}
			set
			{
				AppSettings.AddOrUpdateValue(teeth_cretiria_key, value);
			}
		}

		public static string teethError1Settings
		{
			get
			{
				return AppSettings.GetValueOrDefault(teeth_error1_key, teeth_error1_key_default);
			}
			set
			{
				AppSettings.AddOrUpdateValue(teeth_error1_key, value);
			}
		}

		public static string teethError2Settings
		{
			get
			{
				return AppSettings.GetValueOrDefault(teeth_error2_key, teeth_error2_key_default);
			}
			set
			{
				AppSettings.AddOrUpdateValue(teeth_error2_key, value);
			}
		}
		}

	}


