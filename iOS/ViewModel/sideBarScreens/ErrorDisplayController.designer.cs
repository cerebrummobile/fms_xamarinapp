// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("ErrorDisplayController")]
    partial class ErrorDisplayController
    {
        [Outlet]
        UIKit.UIImageView img_error { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (img_error != null) {
                img_error.Dispose ();
                img_error = null;
            }
        }
    }
}