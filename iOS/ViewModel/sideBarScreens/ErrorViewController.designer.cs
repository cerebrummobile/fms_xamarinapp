// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("ErrorViewController")]
    partial class ErrorViewController
    {
        [Outlet]
        UIKit.UIButton btn_error1 { get; set; }


        [Outlet]
        UIKit.UIButton btn_error2 { get; set; }


        [Outlet]
        UIKit.UIImageView error_img1 { get; set; }


        [Outlet]
        UIKit.UIImageView error_img2 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint K_img_Height { get; set; }


        [Outlet]
        UIKit.UIButton Replace_btn { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btn_error1 != null) {
                btn_error1.Dispose ();
                btn_error1 = null;
            }

            if (btn_error2 != null) {
                btn_error2.Dispose ();
                btn_error2 = null;
            }

            if (error_img1 != null) {
                error_img1.Dispose ();
                error_img1 = null;
            }

            if (error_img2 != null) {
                error_img2.Dispose ();
                error_img2 = null;
            }

            if (K_img_Height != null) {
                K_img_Height.Dispose ();
                K_img_Height = null;
            }

            if (Replace_btn != null) {
                Replace_btn.Dispose ();
                Replace_btn = null;
            }
        }
    }
}