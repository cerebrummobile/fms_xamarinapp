/* This class will display the images related to teeth error with the click listner for the each image
 * 
 */
using Foundation;
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;
using PRAMotionZoom;

namespace IAO
{
	public partial class ErrorViewController : BaseController
    {
        public ErrorViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewWillLayoutSubviews()
		{   
			if (View.Frame.Width == 736 && View.Frame.Height == 414)
			{//6S PLUS
				K_img_Height.Constant = 200;
			}

			if (View.Frame.Width == 667 && View.Frame.Height == 375)
			{//6S
				K_img_Height.Constant = 190;
				//error_img1.h = 300;
			}

			if (View.Frame.Width == 568 && View.Frame.Height == 320)
			{//5S
				K_img_Height.Constant = 150;
			}

			if (View.Frame.Width == 480 && View.Frame.Height == 320)
			{//4s same as 5s

			K_img_Height.Constant = 140;}
			
		}
	
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// set the navigation title 
			Replace_btn.Hidden = true;
			this.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			NavigationItem.Title = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name + " (Error)".ToUpper();

			/// set the right side app icon with clcik listner
			RightBaritemClickListner();
			error_img1.Image = UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error1);
			if (GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error2 != string.Empty)
			{
				error_img2.Image = UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error2);
			}
			btn_error1.SetTitle(" ", UIControlState.Normal);
			btn_error2.SetTitle(" ", UIControlState.Normal);
			var t = error_img1.Center;
			if (GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error2 == string.Empty)
			{   // hide th all the images and disabled the buttons click
				error_img1.Hidden = true;
				error_img2.Hidden = true;
				btn_error1.Enabled = false;
				btn_error2.Enabled = false;
				Replace_btn.SetTitle("", UIControlState.Normal);
				//// call this method for zoom functionality and show th eimage if single image 
				ZoomFunctionalityWithImage();
				//Replace_btn.SetImage(UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error1), UIControlState.Normal);
				//Replace_btn.TouchUpInside += delegate
				//{
				//	// navigation to another view controller 
				//	var Storyboard = UIStoryboard.FromName("Main", null);
				//	ErrorDisplayController displayview = Storyboard.InstantiateViewController("error_displayView") as ErrorDisplayController;
				//	displayview.error_image = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error1;
				//	displayview.errorImage_name = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name + " (Error1)";
				//	NavController.PushViewController(displayview, true);
				//	sidebarController.CloseMenu();

				//};
			}
			// click event for the buttons 
			btn_error1.TouchUpInside += (sender, e) =>
			{   // navigation to another view controller 
				var Storyboard = UIStoryboard.FromName("Main", null);
				ErrorDisplayController displayview = Storyboard.InstantiateViewController("error_displayView") as ErrorDisplayController;
				displayview.error_image = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error1;
				displayview.errorImage_name = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name + " (Error1)".ToUpper();
				NavController.PushViewController(displayview, true);
				sidebarController.CloseMenu();

			};
			btn_error2.TouchUpInside += (sender, e) =>
			{  // navigation to another view controller 
				if (GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error2 != string.Empty)
				{
					var Storyboard = UIStoryboard.FromName("Main", null);
					ErrorDisplayController displayview = Storyboard.InstantiateViewController("error_displayView") as ErrorDisplayController;
					displayview.error_image = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error2;
					displayview.errorImage_name = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name + " (Error2)".ToUpper();
					NavController.PushViewController(displayview, true);
					sidebarController.CloseMenu();
				}



			};


		}

		protected void ZoomFunctionalityWithImage()
		{
			var cgrect_ = new CGRect(0, 20, View.Bounds.Width, View.Bounds.Height - 40);

			//img.Frame = new CGRect(10, 10, View.Bounds.Width - 20, 100 );
			//img.BackgroundColor = UIColor.Red;
			var zoomScrollView = new PRAZoomView(cgrect_, UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_error1));
			zoomScrollView.StartZooming();
			zoomScrollView.Bounces = false;
			zoomScrollView.MaximumZoomScale = 3f;

			zoomScrollView.ZoomScale = 1.0f;
			zoomScrollView.BackgroundColor = UIColor.FromRGB(25, 25, 25);

			zoomScrollView.MinimumZoomScale = 1f;

			zoomScrollView.ContentScaleFactor = 1f;
			zoomScrollView.ScrollsToTop = false;

			var t = zoomScrollView.PinchGestureRecognizer;
			t.Scale = 1f;
			zoomScrollView.SetContentOffset(new CGPoint(zoomScrollView.Bounds.X, zoomScrollView.Bounds.Y), true);
			View.AddSubview(zoomScrollView);
		}
		/// <summary>
		/// set the right side app icon with clcik listner
		/// </summary>
		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				//var Storyboard = UIStoryboard.FromName("Main", null);
				//HomeViewController displayview = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				//NavController.PushViewController(displayview, true);

				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);
				if (device_type.ToString().Equals("Pad"))
				{
					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}

				else
				{
					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}
    }
}