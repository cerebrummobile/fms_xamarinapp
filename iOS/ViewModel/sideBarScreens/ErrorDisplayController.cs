/* This class will display the images for error with zoom functionality 
 * 
 */
using Foundation;
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;
using PRAMotionZoom;

namespace IAO
{
	public partial class ErrorDisplayController : BaseController
	{
		UIPinchGestureRecognizer pinchGesture;
        public ErrorDisplayController (IntPtr handle) : base (handle)
        {
        }
			public string error_image { get; set; }
		public string errorImage_name { get; set; }
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// error_displayView if not empty , set in the view
			if (error_image != string.Empty)
			{
				//img_error.Image = UIImage.FromFile(error_image);
				//img_error.ClipsToBounds = true;
				// call to zoom in and out image
				var cgrect_ = new CGRect(0, 20, View.Bounds.Width, View.Bounds.Height - 30);
				var zoomScrollView = new PRAMotionZoom.PRAZoomView(cgrect_, UIImage.FromFile(error_image));
				zoomScrollView.StartZooming();
				//zoomScrollView.ContentSize = 
				zoomScrollView.Bounces = false;
				zoomScrollView.Bounces = false;
				zoomScrollView.MaximumZoomScale = 3f;

				zoomScrollView.ZoomScale = 1.0f;
				zoomScrollView.BackgroundColor = UIColor.FromRGB(0, 13, 29);

				zoomScrollView.MinimumZoomScale = 1f;

				zoomScrollView.ContentScaleFactor = 1f;
				zoomScrollView.ScrollsToTop = false;

				var t = zoomScrollView.PinchGestureRecognizer;
				t.Scale = 1f;
				zoomScrollView.SetContentOffset(new CGPoint(zoomScrollView.Bounds.X, zoomScrollView.Bounds.Y), true);
				View.AddSubview(zoomScrollView);
				//PinchTozoom(img_error);
			}
			// set the navigation title
			this.NavigationItem.Title = errorImage_name;
			this.NavigationItem.SetLeftBarButtonItem(new UIBarButtonItem(
				UIImage.FromBundle("hamburger"), UIBarButtonItemStyle.Plain, (sender, args) =>
{   sidebarController.ToggleMenu();
	
}), true);

			// set the right side icon 
			RightBaritemClickListner();
			this.NavigationItem.LeftBarButtonItem.Title = "";
			this.NavigationItem.LeftBarButtonItem.TintColor = UIColor.Black;
		}

		/// <summary>
		/// set the app icon at right side with click lisnter
		/// </summary>
		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				//var Storyboard = UIStoryboard.FromName("Main", null);
				//HomeViewController displayview = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				//NavController.PushViewController(displayview, true);
				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);
				if (device_type.ToString().Equals("Pad"))
				{
					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}

				else
				{
					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}

		/// <summary>
		/// pinching to zoom 
		/// </summary>
		/// <param name="view">View.</param>
		protected void PinchTozoom(UIView view)
		{
			pinchGesture = new UIPinchGestureRecognizer(() =>
{
	if (pinchGesture.State == UIGestureRecognizerState.Began || pinchGesture.State == UIGestureRecognizerState.Changed)
	{
		
		if (pinchGesture.Scale < 1.0f)
		{
			pinchGesture.Scale = 1.0f;
		}

		img_error.Transform = CGAffineTransform.MakeScale(pinchGesture.Scale, pinchGesture.Scale);
	}
			});;
			view.AddGestureRecognizer(pinchGesture);
			view.MultipleTouchEnabled = true;
 
		}

    }
}