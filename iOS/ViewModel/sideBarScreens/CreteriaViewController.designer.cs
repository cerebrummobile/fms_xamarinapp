// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("CreteriaViewController")]
    partial class CreteriaViewController
    {
        [Outlet]
        UIKit.UIImageView img_creteria { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (img_creteria != null) {
                img_creteria.Dispose ();
                img_creteria = null;
            }
        }
    }
}