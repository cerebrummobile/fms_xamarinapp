/* This class will display the images for the Anatomy with zoom feature
 * 
 */

using Foundation;
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;
using System.Diagnostics;
using PRAMotionZoom;

namespace IAO
{
	public partial class AnatomyViewController : BaseController
    {
		
		UIPinchGestureRecognizer pinchGesture;
		UIImageView img;

		CGRect cgrect;

		public AnatomyViewController(IntPtr handle) : base(handle)
		{
			
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			// set the image and title for this controller 
			//anatomy_imgview.Image = UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_anatomy);
			NavigationItem.Title = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name +" (Anatomy)".ToUpper() ;
			anatomy_imgview.MultipleTouchEnabled = true;
			img = new UIImageView(UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_anatomy));

			if (View.Frame.Width == 736 && View.Frame.Height == 414)
			{//6S PLU
				cgrect = new CGRect(0, 25, View.Bounds.Width, View.Bounds.Height - 40);
			}

			if (View.Frame.Width == 667 && View.Frame.Height == 375)
			{//6S
				cgrect = new CGRect(0, 14, View.Bounds.Width, View.Bounds.Height - 19);
			}

			if (View.Frame.Width == 568 && View.Frame.Height == 320)
			{//5S
				cgrect = new CGRect(0, 15, View.Bounds.Width, View.Bounds.Height - 22);
			}

			if (View.Frame.Width == 480 && View.Frame.Height == 320)
			{//4s same as 5s
				cgrect = new CGRect(0, 13, View.Bounds.Width, View.Bounds.Height - 22);
			}

			if (device_type.ToString().Equals("Pad"))
			{
				cgrect = new CGRect(0, 30, View.Bounds.Width, View.Bounds.Height - 60);
			}

			var zoomScrollView = new PRAZoomView(cgrect, img.Image);
			this.View.BackgroundColor = UIColor.Blue;
			zoomScrollView.StartZooming();
			zoomScrollView.Bounces = false;
			zoomScrollView.MaximumZoomScale = 3f;

			zoomScrollView.ZoomScale = 1.0f;
			zoomScrollView.BackgroundColor = UIColor.FromRGB(0, 13, 29);

			zoomScrollView.MinimumZoomScale = 1f;

			zoomScrollView.ContentScaleFactor = 1f;
			zoomScrollView.ScrollsToTop = false;

			var t = zoomScrollView.PinchGestureRecognizer;
			t.Scale = 1f;
			zoomScrollView.SetContentOffset(new CGPoint(zoomScrollView.Bounds.X, zoomScrollView.Bounds.Y), true);
			View.AddSubview(zoomScrollView);
			// call to pinch to zoom method
			//PinchTozoom(anatomy_imgview);
			this.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			// set the app icon on right 
			RightBaritemClickListner();
		}
		/// <summary>
		/// set the app icon at right side with click lisnter
		/// </summary>
		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				//var Storyboard = UIStoryboard.FromName("Main", null);
				//HomeViewController displayview = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				//NavController.PushViewController(displayview, true);
				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);

				if (device_type.ToString().Equals("Pad"))
				{
					//var initialViewController = storyBoard.InstantiateViewController("homeScreen_ipad");
					//var NavigationController = new UINavigationController(initialViewController);
					//NavigationController.NavigationBar.BackgroundColor = UIColor.White;

					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();


					//UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
				}

				else
				{

					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

					//var initialViewController = storyBoard.InstantiateViewController("HomeController");
				//	var NavigationController = new UINavigationController(initialViewController);
					//NavigationController.NavigationBar.BackgroundColor = UIColor.White;
				//	UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
				}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}
		/// <summary>
		/// pinching to zoom 
		/// </summary>
		/// <param name="view">View.</param>
		protected void PinchTozoom(UIView view)
		{
			pinchGesture = new UIPinchGestureRecognizer(() =>
{
	if (pinchGesture.State == UIGestureRecognizerState.Began || pinchGesture.State == UIGestureRecognizerState.Changed)
	{

		if (pinchGesture.Scale < 1.0f)
		{
			pinchGesture.Scale = 1.0f;
		}
		view.Transform = CGAffineTransform.MakeScale(pinchGesture.Scale, pinchGesture.Scale);
	}

});
			view.AddGestureRecognizer(pinchGesture);
			view.MultipleTouchEnabled = true;


		}


	}

	}



	
