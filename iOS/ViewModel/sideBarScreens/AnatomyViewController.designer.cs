// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("AnatomyViewController")]
    partial class AnatomyViewController
    {
        [Outlet]
        UIKit.UIImageView anatomy_imgview { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (anatomy_imgview != null) {
                anatomy_imgview.Dispose ();
                anatomy_imgview = null;
            }
        }
    }
}