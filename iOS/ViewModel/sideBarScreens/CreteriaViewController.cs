/* This class will display the images creteria with zoom functionality 
 * 
 */
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;

namespace IAO
{
	public partial class CreteriaViewController : BaseController
    {
		UIPinchGestureRecognizer pinchGesture;
        public CreteriaViewController (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// set the image and title for this controller 
			//img_creteria.Image = UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_cretiria);
			NavigationItem.Title = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name +" (Creteria)".ToUpper();
			//	img_creteria.ClipsToBounds = true;

			// set the app icon at right side with click lisnter
			RightBaritemClickListner();
			// call this function for pinch to zoom and set the image at scrollviewm
			var cgrect_ = new CGRect(0, 20, View.Bounds.Width , View.Bounds.Height);
			var zoomScrollView = new PRAMotionZoom.PRAZoomView(cgrect_, UIImage.FromFile(GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_cretiria));
			zoomScrollView.StartZooming();
			//zoomScrollView.ContentSize = 
			zoomScrollView.Bounces = false;
			zoomScrollView.MaximumZoomScale = 3f;
			zoomScrollView.ZoomScale = 1.0f;
						zoomScrollView.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			zoomScrollView.MinimumZoomScale = 1f;
			zoomScrollView.ContentScaleFactor = 1f;
			zoomScrollView.ScrollsToTop = false;
			var t = zoomScrollView.PinchGestureRecognizer;
			t.Scale = 1f;
			zoomScrollView.SetContentOffset(new CGPoint(zoomScrollView.Bounds.X,zoomScrollView.Bounds.Y), true);
			View.AddSubview(zoomScrollView);
			//PinchTozoom(img_creteria);
		}




		// pinch to zoom function

		protected void PinchTozoom(UIView view)
		{
			pinchGesture = new UIPinchGestureRecognizer(() =>
{
	if (pinchGesture.State == UIGestureRecognizerState.Began || pinchGesture.State == UIGestureRecognizerState.Changed)
	{      

		if (pinchGesture.Scale < 1.0f)
		{
			pinchGesture.Scale = 1.0f;
		}

		view.Transform = CGAffineTransform.MakeScale(pinchGesture.Scale, pinchGesture.Scale);

	}
}); ;
			view.AddGestureRecognizer(pinchGesture);
			view.MultipleTouchEnabled = true;	


		}


		/// <summary>
		/// set the app icon at right side with click lisnter
		/// </summary>
		protected void RightBaritemClickListner()
		{  var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				//var Storyboard = UIStoryboard.FromName("Main", null);
				//HomeViewController displayview = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				//NavController.PushViewController(displayview, true);

				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);

				if (device_type.ToString().Equals("Pad"))
				{
					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}

				else
				{
					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}

    }
}