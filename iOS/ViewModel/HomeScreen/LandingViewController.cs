/* This class will display the images when clicked on home screen image 
 * 
 */
using Foundation;
using System;
using UIKit;
using System.Drawing;
using SidebarNavigation;

namespace IAO
{
	public partial class LandingViewController : BaseController
	{
		public LandingViewController(IntPtr handle) : base(handle)
		{
		}

		public LandingViewController()
		{
		}

		public string landing_img1 { get; set; }
		public string landing_img2 { get; set; }
		public string teeth_name { get; set; }

		public override void ViewWillLayoutSubviews()
		{
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;

			if (device_type.ToString().Equals("Pad"))
			{
				img2_height.Constant = 400f;
				ing_height.Constant = 400f;
			}
		}


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// SET THE DATA INTO THE IMAGES 

			landing_IMG1.Image = UIImage.FromFile(landing_img1);
			if (landing_img2 != string.Empty)
			{
				landing_IMG2.Image = UIImage.FromFile(landing_img2);
				 landing_IMG2.Layer.CornerRadius = 45f;
				// color for the border and width

				landing_IMG2.Layer.BorderColor = UIColor.White.CGColor;
				landing_IMG2.Layer.BorderWidth = 1f;
				landing_IMG2.ClipsToBounds = true;
			}
			this.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			this.NavigationItem.Title = teeth_name.ToUpper();

             landing_IMG1.Layer.CornerRadius = 45f;
			// color for the border and width

			landing_IMG1.Layer.BorderColor = UIColor.White.CGColor;
			landing_IMG1.Layer.BorderWidth = 1f;
			landing_IMG1.ClipsToBounds = true;
			RightBaritemClickListner();
		}

		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				var storyBoard = UIStoryboard.FromName("Main", null);
				if (device_type.ToString().Equals("Pad"))
				{
					HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}

				else
				{
					HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
					NavController.PushViewController(homeviecontroller, true);
					sidebarController.CloseMenu();

				}
			

			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}




	}
}