﻿using System;

using UIKit;
using Foundation;
using System.Drawing;
using CoreGraphics;
using SidebarNavigation;

namespace IAO
{
	public partial class HomeViewController : BaseController
	{
		public static string ButtonClickTag = "";

		public static HomeViewController homescreen;

		//public BaseNavigationController NavControl00ler { get; private set; }
		UIStoryboard storyBoard;
		public HomeViewController(IntPtr handle) : base(handle)
		{

			ButtonClickTag = "";
		}
		public HomeViewController()
		{
			ButtonClickTag = "";
		}
	//	public SidebarController sideBar { get; private set; }

    

		public override void ViewWillLayoutSubviews()
		{
			if (View.Frame.Width == 736 )
			{   //6S PLUS
			     scroll_view.ScrollEnabled = false;
				img_Pa8_left.Constant = 50f;
				pa1_left.Constant = 50f;
				btn_Bw1_left.Constant = 50f;
				bw1_left.Constant = 50f;
				btn_Pa1_left.Constant = 50f;
				btn_PA8_left.Constant = 50f;
			}

		//	scroll_view.BackgroundColor = UIColor.Red;

			//if (View.Frame.Width == 667 && View.Frame.Height == 375)
			//{//6S
			//error_img1.h = 300;

			//scroll_view.Frame = new CGRect(0, -10, scroll_view.Frame.Size.Width, scroll_view.Frame.Size.Height);
			scroll_view.BackgroundColor = UIColor.FromRGB(0, 13, 29);
				scroll_view.ContentSize = new CGSize(650, 310);
		

			//// round the corners of the image 
			img_PA1.Layer.CornerRadius = 10f;
			img_PA2.Layer.CornerRadius = 10f;
			img_PA3.Layer.CornerRadius = 10f;
			img_PA4.Layer.CornerRadius = 10f;
			img_PA5.Layer.CornerRadius = 10f;
			img_PA6.Layer.CornerRadius = 10f;
			IMG_pa7.Layer.CornerRadius = 10f;
			img_PA8.Layer.CornerRadius = 10f;
			img_PA9.Layer.CornerRadius = 10f;
			img_PA10.Layer.CornerRadius = 10f;
			img_PA11.Layer.CornerRadius = 10f;
			img_PA12.Layer.CornerRadius = 10f;
			img_PA13.Layer.CornerRadius = 10f;
			img_PA14.Layer.CornerRadius = 10f;
			img_bw1.Layer.CornerRadius = 10f;
			img_BW2.Layer.CornerRadius = 10f;
			img_BW3.Layer.CornerRadius = 10f;
			img_BW4.Layer.CornerRadius = 10f;


			img_PA1.Layer.BorderColor = UIColor.White.CGColor;
			img_PA1.Layer.BorderWidth = 1f;

			img_PA2.Layer.BorderColor = UIColor.White.CGColor;
			img_PA2.Layer.BorderWidth = 1f;

			img_PA3.Layer.BorderColor = UIColor.White.CGColor;
			img_PA3.Layer.BorderWidth = 1f;

			img_PA4.Layer.BorderColor = UIColor.White.CGColor;
			img_PA4.Layer.BorderWidth = 1f;

			img_PA5.Layer.BorderColor = UIColor.White.CGColor;
			img_PA5.Layer.BorderWidth = 1f;

			img_PA6.Layer.BorderColor = UIColor.White.CGColor;
			img_PA6.Layer.BorderWidth = 1f;

			IMG_pa7.Layer.BorderColor = UIColor.White.CGColor;
			IMG_pa7.Layer.BorderWidth = 1f;

			img_PA8.Layer.BorderColor = UIColor.White.CGColor;
			img_PA8.Layer.BorderWidth = 1f;


			img_PA9.Layer.BorderColor = UIColor.White.CGColor;
			img_PA9.Layer.BorderWidth = 1f;

			img_PA10.Layer.BorderColor = UIColor.White.CGColor;
			img_PA10.Layer.BorderWidth = 1f;

			img_PA11.Layer.BorderColor = UIColor.White.CGColor;
			img_PA11.Layer.BorderWidth = 1f;

			img_PA12.Layer.BorderColor = UIColor.White.CGColor;
			img_PA12.Layer.BorderWidth = 1f;

			img_PA13.Layer.BorderColor = UIColor.White.CGColor;
			img_PA13.Layer.BorderWidth = 1f;

			img_PA14.Layer.BorderColor = UIColor.White.CGColor;
			img_PA14.Layer.BorderWidth = 1f;

			img_bw1.Layer.BorderColor = UIColor.White.CGColor;
			img_bw1.Layer.BorderWidth = 1f;

			img_BW2.Layer.BorderColor = UIColor.White.CGColor;
			img_BW2.Layer.BorderWidth = 1f;

			img_BW3.Layer.BorderColor = UIColor.White.CGColor;
			img_BW3.Layer.BorderWidth = 1f;
			img_BW4.Layer.BorderColor = UIColor.White.CGColor;
			img_BW4.Layer.BorderWidth = 1f;
			//img_PA1.Layer.MasksToBounds = true;
			//img_PA2.Layer.MasksToBounds = true;
			//img_PA3.Layer.MasksToBounds = true;
			//img_PA4.Layer.MasksToBounds = true;
			//img_PA5.Layer.MasksToBounds = true;
			//img_PA6.Layer.MasksToBounds = true;
			//IMG_pa7.Layer.MasksToBounds = true;



			img_PA2.ClipsToBounds = true;
			img_PA3.ClipsToBounds = true;
			img_PA4.ClipsToBounds = true;
			img_PA5.ClipsToBounds = true;
			img_PA6.ClipsToBounds = true;
			IMG_pa7.ClipsToBounds = true;
			img_PA8.ClipsToBounds = true;
			img_PA9.ClipsToBounds = true;
			img_PA10.ClipsToBounds = true;
			img_PA11.ClipsToBounds = true;
			img_PA12.ClipsToBounds = true;
			img_PA13.ClipsToBounds = true;
			img_PA14.ClipsToBounds = true;
			img_bw1.ClipsToBounds = true;
			img_BW2.ClipsToBounds = true;
			img_BW3.ClipsToBounds = true;
			img_BW4.ClipsToBounds = true;




		}
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// set the images in the respectives images UI views 
			SetData();
			homescreen = this;
			storyBoard = UIStoryboard.FromName("Main", null);

			this.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			// app icon 
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 20, 20);

			// info icon on the right side 
			var img_info = UIImage.FromBundle("info_img.png");
			var button_info = UIButton.FromType(UIButtonType.Custom);
			button_info.SetBackgroundImage(img_info, UIControlState.Normal);
			button_info.Frame = new RectangleF(0, 0, 20, 20);

			// click listner for the info button 
			button_info.TouchUpInside += ButtonInfoClickListner;

			//  help icon on the right side 
			var img_hep = UIImage.FromBundle("help_img.png");
			var button_img_hep = UIButton.FromType(UIButtonType.Custom);
			button_img_hep.SetBackgroundImage(img_hep, UIControlState.Normal);
			button_img_hep.Frame = new RectangleF(0, 0, 20, 20);

			// click listner for the help  button 
			button_img_hep.TouchUpInside += HelpButtonClickListner;

			// creating the buttonsitem s for each info and help button 
			var info_buttonItem = new UIBarButtonItem(button_info);
			var help_buttonitem = new UIBarButtonItem(button_img_hep);
			//var homebuttonitem = new  UIBarButtonItem(button);
			var btns = new UIBarButtonItem[] { help_buttonitem, info_buttonItem};
			NavigationItem.SetRightBarButtonItems(btns,true);

			//NavigationItem.RightBarButtonItem = new UIBarButtonItem([button_img_hep, button_info,button]);
			var title = "Tap on tooth to begin";
			NavigationItem.Title = title.ToUpper();

			Console.WriteLine("setting" +Settings.teeth_nameSettings);
			// click events for each image 
			ButtonClickEvents();
			btn_PA1.SetTitle(" ", UIControlState.Normal);
			btn_PA2.SetTitle(" ", UIControlState.Normal);
			btn_PA3.SetTitle(" ", UIControlState.Normal);
			btn_PA4.SetTitle(" ", UIControlState.Normal);
			btn_PA5.SetTitle(" ", UIControlState.Normal);
			btn_PA6.SetTitle(" ", UIControlState.Normal);
			btn_PA7.SetTitle(" ", UIControlState.Normal);
			btn_PA8.SetTitle(" ", UIControlState.Normal);
			btn_PA9.SetTitle(" ", UIControlState.Normal);
			btn_PA10.SetTitle(" ", UIControlState.Normal);
			btn_PA11.SetTitle(" ", UIControlState.Normal);
			btn_PA12.SetTitle(" ", UIControlState.Normal);
			btn_PA13.SetTitle(" ", UIControlState.Normal);
			btn_PA14.SetTitle(" ", UIControlState.Normal);
			btn_BW1.SetTitle(" ", UIControlState.Normal);
			btn_BW2.SetTitle(" ", UIControlState.Normal);
			btn_BW3.SetTitle(" ", UIControlState.Normal);
			btn_BW4.SetTitle(" ", UIControlState.Normal);
		}
		/// <summary>
		///button help click listner.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void HelpButtonClickListner(object sender, EventArgs e)
		{
			ButtonClickTag = "Help_Button";
		//var initialViewController = storyBoard.InstantiateViewController("root");
		//	UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;

			var initialViewController = storyBoard.InstantiateViewController("help_view");
			var NavigationController = new UINavigationController(initialViewController);
			NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;

		}
		/// <summary>
		/// Buttons the info click listner.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void ButtonInfoClickListner(object sender, EventArgs e)
		{
			ButtonClickTag = "info_Button";

			var initialViewController = storyBoard.InstantiateViewController("info_view");
			var NavigationController = new UINavigationController(initialViewController);
			NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;

		//	var initialViewController = storyBoard.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();

		}
		// this method contains the all the images click events and on clicking each image , 
		//takes to the landing screen

		void ButtonClickEvents()
		{  // landing_view
			// PA1 Click event 
			btn_PA1.TouchUpInside += delegate {

				Settings.teeth_nameSettings = "0";
				// call this method to navigte to landing screen
				OpenLandingScreen();

			};
			// PA2 Click event 
			btn_PA2.TouchUpInside += delegate
			{  Settings.teeth_nameSettings = "1";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA3.TouchUpInside += delegate
			{  Settings.teeth_nameSettings = "2";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA4.TouchUpInside += delegate
			{   Settings.teeth_nameSettings = "3";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA5.TouchUpInside += delegate
			{   Settings.teeth_nameSettings = "4";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA6.TouchUpInside += delegate
			{   Settings.teeth_nameSettings = "5";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA7.TouchUpInside += delegate
			{   Settings.teeth_nameSettings = "6";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA8.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "7";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA9.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "8";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA10.TouchUpInside += delegate
			{      Settings.teeth_nameSettings = "9";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA11.TouchUpInside += delegate
			{     Settings.teeth_nameSettings = "10";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA12.TouchUpInside += delegate
			{     Settings.teeth_nameSettings = "11";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA13.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "12";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btn_PA14.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "13";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// bw1 Click event 
			btn_BW1.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "14";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW2 Click event 
			btn_BW2.TouchUpInside += delegate
			{   Settings.teeth_nameSettings = "15";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW3 Click event 
			btn_BW3.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "16";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW4 Click event 
			btn_BW4.TouchUpInside += delegate
			{    Settings.teeth_nameSettings = "17";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};


		}

		protected void OpenLandingScreen()
		{   			
			ButtonClickTag = "HomeImage_button";
			//var storyBoard = UIStoryboard.FromName("Main", null);
			//var initialViewController = storyBoard.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
			var Storyboard = UIStoryboard.FromName("Main", null);
			LandingViewController displayview = Storyboard.InstantiateViewController("landing_view") as LandingViewController;
			displayview.landing_img1 = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing1;
			displayview.landing_img2 = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing2;
			displayview.teeth_name = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name.ToUpper();
			NavController.PushViewController(displayview, true);
			sidebarController.CloseMenu();
		}

		void SetData()
		{   			
			img_PA1.Image = UIImage.FromBundle("PA1.jpg");
			img_PA2.Image = UIImage.FromBundle("PA2.jpg");
			img_PA3.Image = UIImage.FromBundle("PA3.jpg");
			img_PA4.Image = UIImage.FromBundle("PA4.jpg");
			img_PA5.Image = UIImage.FromBundle("PA5.jpg");
			img_PA6.Image = UIImage.FromBundle("PA6.jpg");
			IMG_pa7.Image = UIImage.FromBundle("PA7.jpg");
			img_bw1.Image = UIImage.FromBundle("BW1.jpg");
			img_BW2.Image = UIImage.FromBundle("BW2.jpg");
			img_BW3.Image = UIImage.FromBundle("BW3.jpg");
			img_BW4.Image = UIImage.FromBundle("BW4.jpg");
			img_PA8.Image = UIImage.FromBundle("PA8.jpg");
			img_PA9.Image = UIImage.FromBundle("PA9.jpg");
			img_PA10.Image = UIImage.FromBundle("PA10.jpg");
			img_PA11.Image = UIImage.FromBundle("PA11.jpg");
			img_PA12.Image = UIImage.FromBundle("PA12.jpg");
			img_PA13.Image = UIImage.FromBundle("PA13.jpg");
			img_PA14.Image = UIImage.FromBundle("PA14.jpg");
		
		}



	}
}

