// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("HomeScreenControllerIPaid")]
    partial class HomeScreenControllerIPaid
    {
        [Outlet]
        UIKit.UIButton btn_bw1 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btn_bw1_laeding { get; set; }


        [Outlet]
        UIKit.UIButton btn_bw2 { get; set; }


        [Outlet]
        UIKit.UIButton btn_bw3 { get; set; }


        [Outlet]
        UIKit.UIButton btn_bw4 { get; set; }


        [Outlet]
        UIKit.UIButton btn_pa1 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btn_pa1_laeding { get; set; }


        [Outlet]
        UIKit.UIButton btn_pa2 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa10 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa11 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa12 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa13 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa14 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa3 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa4 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa5 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa6 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa7 { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa8 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btw_pa8_laeding { get; set; }


        [Outlet]
        UIKit.UIButton btw_pa9 { get; set; }


        [Outlet]
        UIKit.UIImageView bw1_IMG { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint bw1_laeding_img { get; set; }


        [Outlet]
        UIKit.UIImageView bw2_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView bw3_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView bw4_IMG { get; set; }


        [Outlet]
        UIKit.UILabel label1 { get; set; }


        [Outlet]
        UIKit.UILabel label10 { get; set; }


        [Outlet]
        UIKit.UILabel label11 { get; set; }


        [Outlet]
        UIKit.UILabel label12 { get; set; }


        [Outlet]
        UIKit.UILabel label13 { get; set; }


        [Outlet]
        UIKit.UILabel label14 { get; set; }


        [Outlet]
        UIKit.UILabel label15 { get; set; }


        [Outlet]
        UIKit.UILabel label16 { get; set; }


        [Outlet]
        UIKit.UILabel label17 { get; set; }


        [Outlet]
        UIKit.UILabel label18 { get; set; }


        [Outlet]
        UIKit.UILabel label2 { get; set; }


        [Outlet]
        UIKit.UILabel label3 { get; set; }


        [Outlet]
        UIKit.UILabel label4 { get; set; }


        [Outlet]
        UIKit.UILabel label5 { get; set; }


        [Outlet]
        UIKit.UILabel label6 { get; set; }


        [Outlet]
        UIKit.UILabel label7 { get; set; }


        [Outlet]
        UIKit.UILabel label8 { get; set; }


        [Outlet]
        UIKit.UILabel label9 { get; set; }


        [Outlet]
        UIKit.UIImageView Pa1_IMG { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint PA1_Leading_img { get; set; }


        [Outlet]
        UIKit.UIImageView pa10_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa11_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa12_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa13_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa14_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa2_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa3_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa4_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa5_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa6_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa7_IMG { get; set; }


        [Outlet]
        UIKit.UIImageView pa8_IMG { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint pa8_img_laeading { get; set; }


        [Outlet]
        UIKit.UIImageView pa9_IMG { get; set; }


        [Outlet]
        UIKit.UIScrollView scroller { get; set; }


        [Outlet]
        UIKit.UIScrollView scrollview_ { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btn_bw1 != null) {
                btn_bw1.Dispose ();
                btn_bw1 = null;
            }

            if (btn_bw1_laeding != null) {
                btn_bw1_laeding.Dispose ();
                btn_bw1_laeding = null;
            }

            if (btn_bw2 != null) {
                btn_bw2.Dispose ();
                btn_bw2 = null;
            }

            if (btn_bw3 != null) {
                btn_bw3.Dispose ();
                btn_bw3 = null;
            }

            if (btn_bw4 != null) {
                btn_bw4.Dispose ();
                btn_bw4 = null;
            }

            if (btn_pa1 != null) {
                btn_pa1.Dispose ();
                btn_pa1 = null;
            }

            if (btn_pa1_laeding != null) {
                btn_pa1_laeding.Dispose ();
                btn_pa1_laeding = null;
            }

            if (btn_pa2 != null) {
                btn_pa2.Dispose ();
                btn_pa2 = null;
            }

            if (btw_pa10 != null) {
                btw_pa10.Dispose ();
                btw_pa10 = null;
            }

            if (btw_pa11 != null) {
                btw_pa11.Dispose ();
                btw_pa11 = null;
            }

            if (btw_pa12 != null) {
                btw_pa12.Dispose ();
                btw_pa12 = null;
            }

            if (btw_pa13 != null) {
                btw_pa13.Dispose ();
                btw_pa13 = null;
            }

            if (btw_pa14 != null) {
                btw_pa14.Dispose ();
                btw_pa14 = null;
            }

            if (btw_pa3 != null) {
                btw_pa3.Dispose ();
                btw_pa3 = null;
            }

            if (btw_pa4 != null) {
                btw_pa4.Dispose ();
                btw_pa4 = null;
            }

            if (btw_pa5 != null) {
                btw_pa5.Dispose ();
                btw_pa5 = null;
            }

            if (btw_pa6 != null) {
                btw_pa6.Dispose ();
                btw_pa6 = null;
            }

            if (btw_pa7 != null) {
                btw_pa7.Dispose ();
                btw_pa7 = null;
            }

            if (btw_pa8 != null) {
                btw_pa8.Dispose ();
                btw_pa8 = null;
            }

            if (btw_pa8_laeding != null) {
                btw_pa8_laeding.Dispose ();
                btw_pa8_laeding = null;
            }

            if (btw_pa9 != null) {
                btw_pa9.Dispose ();
                btw_pa9 = null;
            }

            if (bw1_IMG != null) {
                bw1_IMG.Dispose ();
                bw1_IMG = null;
            }

            if (bw1_laeding_img != null) {
                bw1_laeding_img.Dispose ();
                bw1_laeding_img = null;
            }

            if (bw2_IMG != null) {
                bw2_IMG.Dispose ();
                bw2_IMG = null;
            }

            if (bw3_IMG != null) {
                bw3_IMG.Dispose ();
                bw3_IMG = null;
            }

            if (bw4_IMG != null) {
                bw4_IMG.Dispose ();
                bw4_IMG = null;
            }

            if (label1 != null) {
                label1.Dispose ();
                label1 = null;
            }

            if (label10 != null) {
                label10.Dispose ();
                label10 = null;
            }

            if (label11 != null) {
                label11.Dispose ();
                label11 = null;
            }

            if (label12 != null) {
                label12.Dispose ();
                label12 = null;
            }

            if (label13 != null) {
                label13.Dispose ();
                label13 = null;
            }

            if (label14 != null) {
                label14.Dispose ();
                label14 = null;
            }

            if (label15 != null) {
                label15.Dispose ();
                label15 = null;
            }

            if (label16 != null) {
                label16.Dispose ();
                label16 = null;
            }

            if (label17 != null) {
                label17.Dispose ();
                label17 = null;
            }

            if (label18 != null) {
                label18.Dispose ();
                label18 = null;
            }

            if (label2 != null) {
                label2.Dispose ();
                label2 = null;
            }

            if (label3 != null) {
                label3.Dispose ();
                label3 = null;
            }

            if (label4 != null) {
                label4.Dispose ();
                label4 = null;
            }

            if (label5 != null) {
                label5.Dispose ();
                label5 = null;
            }

            if (label6 != null) {
                label6.Dispose ();
                label6 = null;
            }

            if (label7 != null) {
                label7.Dispose ();
                label7 = null;
            }

            if (label8 != null) {
                label8.Dispose ();
                label8 = null;
            }

            if (label9 != null) {
                label9.Dispose ();
                label9 = null;
            }

            if (Pa1_IMG != null) {
                Pa1_IMG.Dispose ();
                Pa1_IMG = null;
            }

            if (PA1_Leading_img != null) {
                PA1_Leading_img.Dispose ();
                PA1_Leading_img = null;
            }

            if (pa10_IMG != null) {
                pa10_IMG.Dispose ();
                pa10_IMG = null;
            }

            if (pa11_IMG != null) {
                pa11_IMG.Dispose ();
                pa11_IMG = null;
            }

            if (pa12_IMG != null) {
                pa12_IMG.Dispose ();
                pa12_IMG = null;
            }

            if (pa13_IMG != null) {
                pa13_IMG.Dispose ();
                pa13_IMG = null;
            }

            if (pa14_IMG != null) {
                pa14_IMG.Dispose ();
                pa14_IMG = null;
            }

            if (pa2_IMG != null) {
                pa2_IMG.Dispose ();
                pa2_IMG = null;
            }

            if (pa3_IMG != null) {
                pa3_IMG.Dispose ();
                pa3_IMG = null;
            }

            if (pa4_IMG != null) {
                pa4_IMG.Dispose ();
                pa4_IMG = null;
            }

            if (pa5_IMG != null) {
                pa5_IMG.Dispose ();
                pa5_IMG = null;
            }

            if (pa6_IMG != null) {
                pa6_IMG.Dispose ();
                pa6_IMG = null;
            }

            if (pa7_IMG != null) {
                pa7_IMG.Dispose ();
                pa7_IMG = null;
            }

            if (pa8_IMG != null) {
                pa8_IMG.Dispose ();
                pa8_IMG = null;
            }

            if (pa8_img_laeading != null) {
                pa8_img_laeading.Dispose ();
                pa8_img_laeading = null;
            }

            if (pa9_IMG != null) {
                pa9_IMG.Dispose ();
                pa9_IMG = null;
            }

            if (scroller != null) {
                scroller.Dispose ();
                scroller = null;
            }

            if (scrollview_ != null) {
                scrollview_.Dispose ();
                scrollview_ = null;
            }
        }
    }
}