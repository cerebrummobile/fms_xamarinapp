﻿using System;
using System.Drawing;
using CoreGraphics;
using UIKit;

namespace IAO
{
	public partial class HomeScreenControllerIPaid : BaseController
	{
		public HomeScreenControllerIPaid(IntPtr handle) : base(handle)
		{
		}

		public static string buttonclicktag ="";
		UIStoryboard storyBoard;

		public override void ViewWillLayoutSubviews()
		{
			// for ipad 12.9 inch screen 
			if (View.Frame.Width == 1366)
			{
				
				Pa1_IMG.Frame = new CGRect(View.Frame.X + 100, 135, 160, 130);
				btn_pa1.Frame = new CGRect(View.Frame.X + 100, 135, 160, 130);
				label1.Frame = new CGRect(View.Frame.X + 100,Pa1_IMG.Frame.Bottom+6,Pa1_IMG.Frame.Width,22);
				label1.TextAlignment = UITextAlignment.Center;


				pa2_IMG.Frame = new CGRect(Pa1_IMG.Frame.Width + Pa1_IMG.Frame.X + 20, 135, 160, 130);
				btn_pa2.Frame = new CGRect(Pa1_IMG.Frame.Width + Pa1_IMG.Frame.X + 20, 135, 160, 130);

				label2.Frame = new CGRect(Pa1_IMG.Frame.Width + Pa1_IMG.Frame.X + 20, pa2_IMG.Frame.Bottom + 6, pa2_IMG.Frame.Width, 22);
				label2.TextAlignment = UITextAlignment.Center;

				pa3_IMG.Frame = new CGRect(pa2_IMG.Frame.Width + pa2_IMG.Frame.X + 20, 180, 130, 170);
				btw_pa3.Frame = new CGRect(pa2_IMG.Frame.Width + pa2_IMG.Frame.X + 20, 180, 130, 170);

				label3.Frame = new CGRect(pa2_IMG.Frame.Width + pa2_IMG.Frame.X + 20, pa3_IMG.Frame.Bottom + 6, pa3_IMG.Frame.Width, 22);
				label3.TextAlignment = UITextAlignment.Center;

				pa4_IMG.Frame = new CGRect(pa3_IMG.Frame.Width + pa3_IMG.Frame.X + 20, 180, 130, 170);
				btw_pa4.Frame = new CGRect(pa3_IMG.Frame.Width + pa3_IMG.Frame.X + 20, 180, 130, 170);

				label4.Frame = new CGRect(pa3_IMG.Frame.Width + pa3_IMG.Frame.X + 20, pa4_IMG.Frame.Bottom + 6, pa4_IMG.Frame.Width, 22);
				label4.TextAlignment = UITextAlignment.Center;


				pa5_IMG.Frame = new CGRect(pa4_IMG.Frame.Width + pa4_IMG.Frame.X + 20, 180, 130, 170);
				btw_pa5.Frame = new CGRect(pa4_IMG.Frame.Width + pa4_IMG.Frame.X + 20, 180, 130, 170);


				label5.Frame = new CGRect(pa4_IMG.Frame.Width + pa4_IMG.Frame.X + 20, pa5_IMG.Frame.Bottom + 6, pa5_IMG.Frame.Width, 22);
				label5.TextAlignment = UITextAlignment.Center;


				pa6_IMG.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, 135, 160, 130);
				btw_pa6.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, 135, 160, 130);


				label6.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, pa6_IMG.Frame.Bottom + 6, pa6_IMG.Frame.Width, 22);
				label6.TextAlignment = UITextAlignment.Center;


				pa7_IMG.Frame = new CGRect(pa6_IMG.Frame.Width + pa6_IMG.Frame.X + 20, 135, 160, 130);
				btw_pa7.Frame = new CGRect(pa6_IMG.Frame.Width + pa6_IMG.Frame.X + 20, 135, 160, 130);

				label7.Frame = new CGRect(pa6_IMG.Frame.Width + pa6_IMG.Frame.X + 20, pa7_IMG.Frame.Bottom + 6, pa7_IMG.Frame.Width, 22);
				label7.TextAlignment = UITextAlignment.Center;

				bw1_IMG.Frame = new CGRect(View.Frame.X + 100, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);
				btn_bw1.Frame = new CGRect(View.Frame.X + 100, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);

				label8.Frame = new CGRect(View.Frame.X + 100, bw1_IMG.Frame.Bottom + 6, bw1_IMG.Frame.Width, 22);
				label8.TextAlignment = UITextAlignment.Center;

				bw2_IMG.Frame = new CGRect(bw1_IMG.Frame.Width + bw1_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);
				btn_bw2.Frame = new CGRect(bw1_IMG.Frame.Width + bw1_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);

				label9.Frame = new CGRect(bw1_IMG.Frame.Width + bw1_IMG.Frame.X + 20, bw2_IMG.Frame.Bottom + 6, bw2_IMG.Frame.Width, 22);
				label9.TextAlignment = UITextAlignment.Center;

				bw3_IMG.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);
				btn_bw3.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);

				label10.Frame = new CGRect(pa5_IMG.Frame.Width + pa5_IMG.Frame.X + 20, bw3_IMG.Frame.Bottom + 6, bw3_IMG.Frame.Width, 22);
				label10.TextAlignment = UITextAlignment.Center;

				bw4_IMG.Frame = new CGRect(bw3_IMG.Frame.Width + bw3_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);
				btn_bw4.Frame = new CGRect(bw3_IMG.Frame.Width + bw3_IMG.Frame.X + 20, Pa1_IMG.Frame.Height + Pa1_IMG.Frame.Y + 60, 160, 130);

				label11.Frame = new CGRect(bw3_IMG.Frame.Width + bw3_IMG.Frame.X + 20, bw4_IMG.Frame.Bottom + 6, bw4_IMG.Frame.Width, 22);
				label11.TextAlignment = UITextAlignment.Center;


				pa8_IMG.Frame = new CGRect(View.Frame.X + 100, bw1_IMG.Frame.Height + bw1_IMG.Frame.Y + 60, 160, 130);


				btw_pa8.Frame = new CGRect(View.Frame.X + 100, bw1_IMG.Frame.Height + bw1_IMG.Frame.Y + 60, 160, 130);

				label12.Frame = new CGRect(View.Frame.X + 100, pa8_IMG.Frame.Bottom + 6, pa8_IMG.Frame.Width, 22);
				label12.TextAlignment = UITextAlignment.Center;

				pa9_IMG.Frame = new CGRect(pa8_IMG.Frame.Width + pa8_IMG.Frame.X + 20, bw1_IMG.Frame.Height + bw1_IMG.Frame.Y + 60, 160, 130);
				btw_pa9.Frame = new CGRect(pa8_IMG.Frame.Width + pa8_IMG.Frame.X + 20, bw1_IMG.Frame.Height + bw1_IMG.Frame.Y + 60, 160, 130);

				label13.Frame = new CGRect(pa8_IMG.Frame.Width + pa8_IMG.Frame.X + 20, pa9_IMG.Frame.Bottom + 6, pa9_IMG.Frame.Width, 22);
				label13.TextAlignment = UITextAlignment.Center;


				pa10_IMG.Frame = new CGRect(pa9_IMG.Frame.Width + pa9_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);
			btw_pa10.Frame = new CGRect(pa9_IMG.Frame.Width + pa9_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);
			
				label14.Frame = new CGRect(pa9_IMG.Frame.Width + pa9_IMG.Frame.X + 20, pa10_IMG.Frame.Bottom + 6, pa10_IMG.Frame.Width, 22);
				label14.TextAlignment = UITextAlignment.Center;

				pa11_IMG.Frame = new CGRect(pa10_IMG.Frame.Width + pa10_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);
				btw_pa11.Frame = new CGRect(pa10_IMG.Frame.Width + pa10_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);

				label15.Frame = new CGRect(pa10_IMG.Frame.Width + pa10_IMG.Frame.X + 20, pa11_IMG.Frame.Bottom + 6, pa11_IMG.Frame.Width, 22);
				label15.TextAlignment = UITextAlignment.Center;



				pa12_IMG.Frame = new CGRect(pa11_IMG.Frame.Width + pa11_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);
				btw_pa12.Frame = new CGRect(pa11_IMG.Frame.Width + pa11_IMG.Frame.X + 20, pa3_IMG.Frame.Height + pa3_IMG.Frame.Y + 60, 130, 170);

				label16.Frame = new CGRect(pa11_IMG.Frame.Width + pa11_IMG.Frame.X + 20, pa12_IMG.Frame.Bottom + 6, pa12_IMG.Frame.Width, 22);
				label16.TextAlignment = UITextAlignment.Center;

				pa13_IMG.Frame = new CGRect(pa12_IMG.Frame.Width + pa12_IMG.Frame.X + 20, bw3_IMG.Frame.Height + bw3_IMG.Frame.Y + 60, 160, 130);
				btw_pa13.Frame = new CGRect(pa12_IMG.Frame.Width + pa12_IMG.Frame.X + 20, bw3_IMG.Frame.Height + bw3_IMG.Frame.Y + 60, 160, 130);

				label17.Frame = new CGRect(pa12_IMG.Frame.Width + pa12_IMG.Frame.X + 20, pa13_IMG.Frame.Bottom + 6, pa13_IMG.Frame.Width, 22);
				label17.TextAlignment = UITextAlignment.Center;


				pa14_IMG.Frame = new CGRect(pa13_IMG.Frame.Width + pa13_IMG.Frame.X + 20, bw3_IMG.Frame.Height + bw3_IMG.Frame.Y + 60, 160, 130);
				btw_pa14.Frame = new CGRect(pa13_IMG.Frame.Width + pa13_IMG.Frame.X + 20, bw3_IMG.Frame.Height + bw3_IMG.Frame.Y + 60, 160, 130);

				label18.Frame = new CGRect(pa13_IMG.Frame.Width + pa13_IMG.Frame.X + 20, pa14_IMG.Frame.Bottom + 6, pa14_IMG.Frame.Width, 22);
				label18.TextAlignment = UITextAlignment.Center;


			}
		}
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetData();
			//var t = View.Frame.Height;
			storyBoard = UIStoryboard.FromName("Main", null);

			scrollview_.ScrollEnabled = false;
			this.View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			scroller.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			// app icon 
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new RectangleF(0, 0, 30, 30);
			// info icon on the right side 
			var img_info = UIImage.FromBundle("info_img.png");
			var button_info = UIButton.FromType(UIButtonType.Custom);
			button_info.SetBackgroundImage(img_info, UIControlState.Normal);
			button_info.Frame = new RectangleF(0, 0, 20, 20);

			// click listner for the info button 
			button_info.TouchUpInside += ButtonInfoClickListner;

			//  help icon on the right side 
			var img_hep = UIImage.FromBundle("help_img.png");
			var button_img_hep = UIButton.FromType(UIButtonType.Custom);
			button_img_hep.SetBackgroundImage(img_hep, UIControlState.Normal);
			button_img_hep.Frame = new RectangleF(0, 0, 20, 20);

			// click listner for the help  button 
			button_img_hep.TouchUpInside += HelpButtonClickListner;

			// creating the buttonsitem s for each info and help button 
			var info_buttonItem = new UIBarButtonItem(button_info);
			var help_buttonitem = new UIBarButtonItem(button_img_hep);
			//var homebuttonitem = new  UIBarButtonItem(button);
			var btns = new UIBarButtonItem[] { help_buttonitem, info_buttonItem };
			NavigationItem.SetRightBarButtonItems(btns, true);
			var title = "Tap on tooth to begin";
			NavigationItem.Title = title.ToUpper();
			//NavigationItem.RightBarButtonItem.

			// click listners for the button 
			ButtonClickEvents();
		}

		/// <summary>
		///button help click listner.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void HelpButtonClickListner(object sender, EventArgs e)
		{
			buttonclicktag = "Help_Button";
			//var initialViewController = storyBoard.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
			var initialViewController = storyBoard.InstantiateViewController("helpscreen_ipad");
			var NavigationController = new UINavigationController(initialViewController);
			NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
			//var initialViewController = storyBoard.InstantiateViewController("help_view");
			//var NavigationController = new UINavigationController(initialViewController);
			//NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			//UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
		}
		/// <summary>
		/// Buttons the info click listner.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void ButtonInfoClickListner(object sender, EventArgs e)
		{
			buttonclicktag = "info_Button";
			//var initialViewController = storyBoard.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;

			var initialViewController = storyBoard.InstantiateViewController("info_view");
			var NavigationController = new UINavigationController(initialViewController);
			NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
		}


		void SetData()
		{
			Pa1_IMG.Image = UIImage.FromBundle("PA1.jpg");
			pa2_IMG.Image = UIImage.FromBundle("PA2.jpg");
			pa3_IMG.Image = UIImage.FromBundle("PA3.jpg");
			pa4_IMG.Image = UIImage.FromBundle("PA4.jpg");
			pa5_IMG.Image = UIImage.FromBundle("PA5.jpg");
			pa6_IMG.Image = UIImage.FromBundle("PA6.jpg");
			pa7_IMG.Image = UIImage.FromBundle("PA7.jpg");
			bw1_IMG.Image = UIImage.FromBundle("BW1.jpg");
			bw2_IMG.Image = UIImage.FromBundle("BW2.jpg");
			bw3_IMG.Image = UIImage.FromBundle("BW3.jpg");
			bw4_IMG.Image = UIImage.FromBundle("BW4.jpg");
			pa8_IMG.Image = UIImage.FromBundle("PA8.jpg");
			pa9_IMG.Image = UIImage.FromBundle("PA9.jpg");
			pa10_IMG.Image = UIImage.FromBundle("PA10.jpg");
			pa11_IMG.Image = UIImage.FromBundle("PA11.jpg");
			pa12_IMG.Image = UIImage.FromBundle("PA12.jpg");
			pa13_IMG.Image = UIImage.FromBundle("PA13.jpg");
			pa14_IMG.Image = UIImage.FromBundle("PA14.jpg");
			// make title empty for buttons

			btn_pa1.SetTitle(" ", UIControlState.Normal);
			btn_pa2.SetTitle(" ", UIControlState.Normal);
			btw_pa3.SetTitle(" ", UIControlState.Normal);
			btw_pa4.SetTitle(" ", UIControlState.Normal);
			btw_pa5.SetTitle(" ", UIControlState.Normal);
			btw_pa6.SetTitle(" ", UIControlState.Normal);
			btw_pa7.SetTitle(" ", UIControlState.Normal);
			btw_pa8.SetTitle(" ", UIControlState.Normal);
			btw_pa9.SetTitle(" ", UIControlState.Normal);
			btw_pa10.SetTitle(" ", UIControlState.Normal);
			btw_pa11.SetTitle(" ", UIControlState.Normal);
			btw_pa12.SetTitle(" ", UIControlState.Normal);
			btw_pa13.SetTitle(" ", UIControlState.Normal);
			btw_pa14.SetTitle(" ", UIControlState.Normal);
			btn_bw1.SetTitle(" ", UIControlState.Normal);
			btn_bw2.SetTitle(" ", UIControlState.Normal);
			btn_bw3.SetTitle(" ", UIControlState.Normal);
			btn_bw4.SetTitle(" ", UIControlState.Normal);
			//// round the corners of the image 
			Pa1_IMG.Layer.CornerRadius = 20f;
			pa2_IMG.Layer.CornerRadius = 20f;
			pa3_IMG.Layer.CornerRadius = 20f;
			pa4_IMG.Layer.CornerRadius = 20f;
			pa5_IMG.Layer.CornerRadius = 20f;
			pa6_IMG.Layer.CornerRadius = 20f;
			pa7_IMG.Layer.CornerRadius = 20f;
			bw1_IMG.Layer.CornerRadius = 20f;
			bw2_IMG.Layer.CornerRadius = 20f;
			bw3_IMG.Layer.CornerRadius = 20f;
			bw4_IMG.Layer.CornerRadius = 20f;
			pa8_IMG.Layer.CornerRadius = 20f;
			pa9_IMG.Layer.CornerRadius = 20f;
			pa10_IMG.Layer.CornerRadius = 20f;
			pa11_IMG.Layer.CornerRadius = 20f;
			pa12_IMG.Layer.CornerRadius = 20f;
			pa13_IMG.Layer.CornerRadius = 20f;
			pa14_IMG.Layer.CornerRadius = 20f;


			// color for the border and width

			Pa1_IMG.Layer.BorderColor = UIColor.White.CGColor;
			Pa1_IMG.Layer.BorderWidth = 1f;

			pa2_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa2_IMG.Layer.BorderWidth = 1f;

			pa3_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa3_IMG.Layer.BorderWidth = 1f;

			pa4_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa4_IMG.Layer.BorderWidth = 1f;

			pa5_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa5_IMG.Layer.BorderWidth = 1f;

			pa6_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa6_IMG.Layer.BorderWidth = 1f;

			pa7_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa7_IMG.Layer.BorderWidth = 1f;

			pa8_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa8_IMG.Layer.BorderWidth = 1f;


			pa9_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa9_IMG.Layer.BorderWidth = 1f;

			pa10_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa10_IMG.Layer.BorderWidth = 1f;

			pa11_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa11_IMG.Layer.BorderWidth = 1f;

			pa12_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa12_IMG.Layer.BorderWidth = 1f;

			pa13_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa13_IMG.Layer.BorderWidth = 1f;

			pa14_IMG.Layer.BorderColor = UIColor.White.CGColor;
			pa14_IMG.Layer.BorderWidth = 1f;

			bw1_IMG.Layer.BorderColor = UIColor.White.CGColor;
			bw1_IMG.Layer.BorderWidth = 1f;

			bw2_IMG.Layer.BorderColor = UIColor.White.CGColor;
			bw2_IMG.Layer.BorderWidth = 1f;

			bw3_IMG.Layer.BorderColor = UIColor.White.CGColor;
			bw3_IMG.Layer.BorderWidth = 1f;
			bw4_IMG.Layer.BorderColor = UIColor.White.CGColor;
			bw4_IMG.Layer.BorderWidth = 1f;

			Pa1_IMG.ClipsToBounds = true;
			pa2_IMG.ClipsToBounds = true;
			pa3_IMG.ClipsToBounds = true;
			pa4_IMG.ClipsToBounds = true;
			pa5_IMG.ClipsToBounds = true;
			pa6_IMG.ClipsToBounds = true;
			pa7_IMG.ClipsToBounds = true;
			pa8_IMG.ClipsToBounds = true;
			pa9_IMG.ClipsToBounds = true;
			pa10_IMG.ClipsToBounds = true;
			pa11_IMG.ClipsToBounds = true;
			pa12_IMG.ClipsToBounds = true;
			pa13_IMG.ClipsToBounds = true;
			bw1_IMG.ClipsToBounds = true;
			bw2_IMG.ClipsToBounds = true;
			bw3_IMG.ClipsToBounds = true;
			bw4_IMG.ClipsToBounds = true;



		}

		void ButtonClickEvents()
		{// landing_view
		 // PA1 Click event 
			btn_pa1.TouchUpInside += delegate
			{

				Settings.teeth_nameSettings = "0";
				// call this method to navigte to landing screen
				OpenLandingScreen();

			};
			// PA2 Click event 
			btn_pa2.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "1";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa3.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "2";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa4.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "3";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa5.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "4";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa6.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "5";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa7.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "6";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa8.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "7";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa9.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "8";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa10.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "9";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa11.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "10";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa12.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "11";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa13.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "12";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// PA1 Click event 
			btw_pa14.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "13";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// bw1 Click event 
			btn_bw1.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "14";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW2 Click event 
			btn_bw2.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "15";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW3 Click event 
			btn_bw3.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "16";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};
			// BW4 Click event 
			btn_bw4.TouchUpInside += delegate
			{
				Settings.teeth_nameSettings = "17";
				// call this method to navigte to landing screen
				OpenLandingScreen();
			};


		}

		protected void OpenLandingScreen()
		{
			var storyBoard = UIStoryboard.FromName("Main", null);
			//var initialViewController = storyBoard.InstantiateViewController("root");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
			LandingViewController displayview = Storyboard.InstantiateViewController("landing_view") as LandingViewController;
			displayview.landing_img1 = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing1;
			displayview.landing_img2 = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing2;
			displayview.teeth_name = GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name.ToUpper();
			NavController.PushViewController(displayview, true);
			sidebarController.CloseMenu();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

