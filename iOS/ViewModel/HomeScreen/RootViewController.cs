﻿/* This class is setting the main screen as home screen and slidebar with the content
 * 
 */
using System;
using SidebarNavigation;
using UIKit;

namespace IAO
{
	public partial class RootViewController : UIViewController
	{
		static UIWindow mWindow;

		public SidebarController sideBar { get; private set; }

		public RootViewController() 
		{

		}
 
		string buttonTag = "landing_view";

		public BaseNavigationController NavController { get; private set; }

        public RootViewController(IntPtr handle) : base(handle)
		{
         }

		/// <summary>
		/// Views will be loaded in the controller with slidebar and landing sreen if images under home is clicked 
		/// </summary>
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			/// initialised the basecontroller class for navigation bar 
			NavController = new BaseNavigationController();
			var Storyboard = UIStoryboard.FromName("Main", null);
			// call the sidebar controller class 
			var menu = Storyboard.InstantiateViewController("menucontroller") as SideMenuController;

			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			Console.WriteLine(device_type);
			if (device_type.ToString().Equals("Pad"))
			{
				var content = Storyboard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
				NavController.PushViewController(content, false);
			}
			else
			{
				var content = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				NavController.PushViewController(content, false);

		}
			//var initialViewController = Storyboard.InstantiateViewController("info_view");
			//var NavigationController = new UINavigationController(initialViewController);
			//NavigationController.NavigationBar.BackgroundColor = UIColor.White;
			//UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;

		//	var content = Storyboard.InstantiateViewController("landing_view") as LandingViewController;
		//		content.landing_img1 = BaseController.GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing1;
			//	content.landing_img2 = BaseController.GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_landing2;
			//	content.teeth_name = BaseController.GetTeethsDataList[Int32.Parse(Settings.teeth_nameSettings)].teeth_name;
			//	NavController.PushViewController(content, false);


			try {
				//mWindow = new UIWindow(UIScreen.MainScreen.Bounds);

				//Menu = new SlideoutNavigationController();
				//Menu.MainViewController = new MainNavigationController(new LandingViewController(), Menu);
				//Menu.MenuViewController = new MenuNavigationController(new SideMenuController(), Menu) { NavigationBarHidden = true };
				//mWindow.RootViewController = Menu;
				//mWindow.MakeKeyAndVisible();
				//window = new UIWindow(UIScreen.MainScreen.Bounds);
				////Instantiate the controller
				//var slideoutNav = new SlideoutNavigationController();

				////Assign the menu ViewController to our nib controller
				////See the sample project for a more detailed menu controller
				////such as a tableview using MonoTouch.Dialog
				//slideoutNav.MenuViewController = new MenuNavigationController(menu, slideoutNav);

				////Assign the Top view controller which is seen first before
				////you slide over to see the menu.
				//slideoutNav.MainViewController = new MenuNavigationController(content, slideoutNav); 

				////Assign the controller to be displayed!
				//window.RootViewController = slideoutNav;
				//window.MakeKeyAndVisible();
				//var content = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
				sideBar = new  SidebarNavigation.SidebarController(this, NavController, menu);
				sideBar.MenuWidth = 240;
				sideBar.ReopenOnRotate = true;
				sideBar.MenuLocation = SidebarController.MenuLocations.Left;
				sideBar.GestureActiveArea = 100;

			}
			catch (NullReferenceException exp)
			{
				Console.WriteLine(exp);
			}

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();

		}
	}
}

