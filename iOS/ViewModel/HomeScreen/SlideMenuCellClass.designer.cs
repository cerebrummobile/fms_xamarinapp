// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace IAO
{
    [Register ("SlideMenuCellClass")]
    partial class SlideMenuCellClass
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Icon_slidemenu { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel text_slidemenu { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Icon_slidemenu != null) {
                Icon_slidemenu.Dispose ();
                Icon_slidemenu = null;
            }

            if (text_slidemenu != null) {
                text_slidemenu.Dispose ();
                text_slidemenu = null;
            }
        }
    }
}