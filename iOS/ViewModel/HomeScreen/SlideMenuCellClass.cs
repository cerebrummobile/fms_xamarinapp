using Foundation;
using System;
using UIKit;

namespace IAO
{
    public partial class SlideMenuCellClass : UITableViewCell
    {
		

		public SlideMenuCellClass (IntPtr handle) : base(handle)
        {
        }

		 public SlideMenuCellClass (NSString cellId) : base (UITableViewCellStyle.Default, cellId)
        {
			//text_slidemenu.TextColor = UIColor.Black;

		 }
		public SlideMenuCellClass()
		{
		}
		 public void UpdateCell(string caption, string subtitle, UIImage image)
		{
			Icon_slidemenu.Image = image;
			var imageSelected = image;
			text_slidemenu.Text = caption;
			text_slidemenu.TextColor = UIColor.White;
			text_slidemenu.HighlightedTextColor = UIColor.Black;
			if (Icon_slidemenu.Highlighted) {

				Icon_slidemenu.TintColor = UIColor.Black;
			}
		}
	}
}