// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("LandingViewController")]
    partial class LandingViewController
    {
        [Outlet]
        UIKit.NSLayoutConstraint img2_height { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint ing_height { get; set; }


        [Outlet]
        UIKit.UIImageView landing_IMG1 { get; set; }


        [Outlet]
        UIKit.UIImageView landing_IMG2 { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (img2_height != null) {
                img2_height.Dispose ();
                img2_height = null;
            }

            if (ing_height != null) {
                ing_height.Dispose ();
                ing_height = null;
            }

            if (landing_IMG1 != null) {
                landing_IMG1.Dispose ();
                landing_IMG1 = null;
            }

            if (landing_IMG2 != null) {
                landing_IMG2.Dispose ();
                landing_IMG2 = null;
            }
        }
    }
}