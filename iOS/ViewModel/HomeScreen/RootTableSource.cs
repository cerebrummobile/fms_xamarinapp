﻿/* This class is upadting the tableviw with the items .
 * 
 */
using System;
using CoreGraphics;
using FMS_IAO.iOS;
using Foundation;
using UIKit;

namespace IAO
{
	public class RootTableSource : UITableViewSource
	{

		// there is NO database or storage of Tasks in this example, just an in-memory List<>
		NSString cellIdentifier = new NSString("cell");
		//string cellIdentifier = "TableCell"; // set in the Storyboard
		string[] str_list;
		string[] image_list;
		SideMenuController sideMenuController;

		public RootTableSource()
		{

		}

		public RootTableSource(string[] str_list, string[] image_list, SideMenuController sideMenuController)
		{
			this.str_list = str_list;
			this.image_list = image_list;
			this.sideMenuController = sideMenuController;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return str_list.Length;
		}
		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			//UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

			//var item = str_list[indexPath.Row];
			//var image = image_list[indexPath.Row];
			//cell = null;
			//////---- if there are no cells to reuse, create a new one
			//if (cell == null)
			//{
			//	cell = new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);
			//}
			//cell.ImageView.Image = UIImage.FromBundle(image);
			//cell.TextLabel.Text = item;
			//cell.BackgroundColor = UIColor.White;
			//cell.TextLabel.TextRectForBounds(new CGRect(10, 0, 50, 40),1);
			////CGFloat spacing = 10;
			//cell.TextLabel.TextAlignment = UITextAlignment.Left;
			//cell.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);

			var cell = tableView.DequeueReusableCell(cellIdentifier) as SlideMenuCellClass;
			if (cell == null)
				cell = new SlideMenuCellClass(cellIdentifier);
			cell.UpdateCell(str_list[indexPath.Row]
					, str_list[indexPath.Row]
							, UIImage.FromBundle(image_list[indexPath.Row]));
			cell.BackgroundColor = UIColor.Black;
			//UIView selctedcolr = new UIView();
			//selctedcolr.BackgroundColor = UIColor.FromRGBA(255, 255, 255, 0.0f);

			//cell.SelectedBackgroundView = selctedcolr;
		// use UITableViewCell.appearance() to configure 
		// the default appearance of all UITableViewCells in your app
			return cell;
		}


		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.CellAt(indexPath);


			// this method will handle the click events over the menu items 
			NavigateViews(cell, indexPath, tableView);
		}

		void NavigateViews(UITableViewCell cell, NSIndexPath indexPath, UITableView tableView)
		{
			var Storyboard = UIStoryboard.FromName("Main", null);
			switch (indexPath.Row)
			{

				case 0:

					sideMenuController.InvokeOnMainThread(delegate
						{
							//NavController.PushViewController(displayview, true);
							var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
							var storyBoard = UIStoryboard.FromName("Main", null);
							if (device_type.ToString().Equals("Pad"))
							{
								HomeScreenControllerIPaid homeviecontroller = storyBoard.InstantiateViewController("homeScreen_ipad") as HomeScreenControllerIPaid;
								sideMenuController.NavController.PushViewController(homeviecontroller, true);
								sideMenuController.sidebarController.CloseMenu();

							}

							else
							{
								HomeViewController homeviecontroller = storyBoard.InstantiateViewController("HomeController") as HomeViewController;
								sideMenuController.NavController.PushViewController(homeviecontroller, true);
								sideMenuController.sidebarController.CloseMenu();

							}
						 
						});
					break;


				case 1:
					sideMenuController.InvokeOnMainThread(delegate
						{
							AcquisitionViewController acquisitionview = Storyboard.InstantiateViewController("acquisition_view") as AcquisitionViewController;
							sideMenuController.NavController.PushViewController(acquisitionview, true);
							sideMenuController.sidebarController.CloseMenu();

						});
					break;

				case 2:
					sideMenuController.InvokeOnMainThread(delegate
						{

							AnatomyViewController anatomyview = Storyboard.InstantiateViewController("anatomy_view") as AnatomyViewController;
							sideMenuController.NavController.PushViewController(anatomyview, true);
							sideMenuController.sidebarController.CloseMenu();

						});
					break;

				case 3:
					sideMenuController.InvokeOnMainThread(delegate
						{

							CreteriaViewController cretiriaview = Storyboard.InstantiateViewController("creteria_view") as CreteriaViewController;
							sideMenuController.NavController.PushViewController(cretiriaview, true);
							sideMenuController.sidebarController.CloseMenu();

						});
					break;

				case 4:
					sideMenuController.InvokeOnMainThread(delegate
						{
							ErrorViewController homeview = Storyboard.InstantiateViewController("error_view") as ErrorViewController;
							sideMenuController.NavController.PushViewController(homeview, true);
							sideMenuController.sidebarController.CloseMenu();

						});
					break;

				case 5:
					sideMenuController.InvokeOnMainThread(delegate
						{

							InfoViewController homeview = Storyboard.InstantiateViewController("info_view") as InfoViewController;
							sideMenuController.NavController.PushViewController(homeview, true);
							sideMenuController.sidebarController.CloseMenu();

						});
					break;

				case 6:

					sideMenuController.InvokeOnMainThread(delegate
						{
						      
						var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
							var storyBoard = UIStoryboard.FromName("Main", null);
							if (device_type.ToString().Equals("Pad"))
							{
							  HelpScreenIPadController   homeview = Storyboard.InstantiateViewController("helpscreen_ipad") as HelpScreenIPadController;
								sideMenuController.NavController.PushViewController(homeview, true);
								sideMenuController.sidebarController.CloseMenu();

							}

							else
							{
								HelpViewController homeview = Storyboard.InstantiateViewController("help_view") as HelpViewController;
								sideMenuController.NavController.PushViewController(homeview, true);
								sideMenuController.sidebarController.CloseMenu();


							}




						});
					break;
			}
		}

		public string GetItem(int id)
		{
			return str_list[id];
		}
	}
}
