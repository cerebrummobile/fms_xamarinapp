/* This class will display the all the menu icons under the tableview 
 * 
 */
using Foundation;
using System;
using UIKit;

namespace IAO
{
	public partial class SideMenuController : BaseController
    {    string[] image_list = {"HOME" ,"Acquisition", "Anatomy", "Criteria", "Errors" ,"info_menu","help_menu"};
		UIView header_view, footer_view;
		string[] str_list = { "Home", "Acquisition", "Anatomy", "Criteria", "Error","Information" , "Help"};
		public SideMenuController (IntPtr handle) : base(handle)
        {
        }

		//public SideMenuController() { }
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			//this.View.BackgroundColor = UIColor.FromRGB(206, 211, 217);
			tableview.Source = new RootTableSource(str_list, image_list, this);
			tableview.SeparatorColor = UIColor.White;
			tableview.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			tableview.BackgroundColor = UIColor.Black;

			tableview.Bounces = false;
			// set the haeder height in case of ipad 
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			if (device_type.ToString().Equals("Pad"))
			{
				header_view = new UIView(new System.Drawing.RectangleF(0, 0, (float)this.View.Bounds.Width, 40f));
				header_view.BackgroundColor = UIColor.Black;
				tableview.TableHeaderView = header_view;
			}
			else
			{
				header_view = new UIView(new System.Drawing.RectangleF(0, 0, (float)this.View.Bounds.Width, 30f));
				header_view.BackgroundColor = UIColor.Black;
				tableview.TableHeaderView = header_view;
			}
			// define the footer view for removing the separtor lines 
			footer_view = new UIView(new System.Drawing.RectangleF(0, 0, (float)this.View.Bounds.Width,20f));
			footer_view.BackgroundColor = UIColor.Black;
			tableview.TableFooterView = footer_view;

		}





		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();

		}


    }
}