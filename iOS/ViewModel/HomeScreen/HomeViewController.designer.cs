// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IAO
{
    [Register ("HomeViewController")]
    partial class HomeViewController
    {
        [Outlet]
        UIKit.UIButton btn_BW1 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btn_Bw1_left { get; set; }


        [Outlet]
        UIKit.UIButton btn_BW2 { get; set; }


        [Outlet]
        UIKit.UIButton btn_BW3 { get; set; }


        [Outlet]
        UIKit.UIButton btn_BW4 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA1 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btn_Pa1_left { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA10 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA11 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA12 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA13 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA14 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA2 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA3 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA4 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA5 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA6 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA7 { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA8 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint btn_PA8_left { get; set; }


        [Outlet]
        UIKit.UIButton btn_PA9 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint bw1_left { get; set; }


        [Outlet]
        UIKit.UIImageView img_bw1 { get; set; }


        [Outlet]
        UIKit.UIImageView img_BW2 { get; set; }


        [Outlet]
        UIKit.UIImageView img_BW3 { get; set; }


        [Outlet]
        UIKit.UIImageView img_BW4 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA1 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA10 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA11 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA12 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA13 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA14 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA2 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA3 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA4 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA5 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA6 { get; set; }


        [Outlet]
        UIKit.UIImageView IMG_pa7 { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA8 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint img_Pa8_left { get; set; }


        [Outlet]
        UIKit.UIImageView img_PA9 { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint pa1_left { get; set; }


        [Outlet]
        UIKit.UIScrollView scroll_view { get; set; }


        [Action ("actionMenuButton:")]
        partial void actionMenuButton (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (btn_BW1 != null) {
                btn_BW1.Dispose ();
                btn_BW1 = null;
            }

            if (btn_Bw1_left != null) {
                btn_Bw1_left.Dispose ();
                btn_Bw1_left = null;
            }

            if (btn_BW2 != null) {
                btn_BW2.Dispose ();
                btn_BW2 = null;
            }

            if (btn_BW3 != null) {
                btn_BW3.Dispose ();
                btn_BW3 = null;
            }

            if (btn_BW4 != null) {
                btn_BW4.Dispose ();
                btn_BW4 = null;
            }

            if (btn_PA1 != null) {
                btn_PA1.Dispose ();
                btn_PA1 = null;
            }

            if (btn_Pa1_left != null) {
                btn_Pa1_left.Dispose ();
                btn_Pa1_left = null;
            }

            if (btn_PA10 != null) {
                btn_PA10.Dispose ();
                btn_PA10 = null;
            }

            if (btn_PA11 != null) {
                btn_PA11.Dispose ();
                btn_PA11 = null;
            }

            if (btn_PA12 != null) {
                btn_PA12.Dispose ();
                btn_PA12 = null;
            }

            if (btn_PA13 != null) {
                btn_PA13.Dispose ();
                btn_PA13 = null;
            }

            if (btn_PA14 != null) {
                btn_PA14.Dispose ();
                btn_PA14 = null;
            }

            if (btn_PA2 != null) {
                btn_PA2.Dispose ();
                btn_PA2 = null;
            }

            if (btn_PA3 != null) {
                btn_PA3.Dispose ();
                btn_PA3 = null;
            }

            if (btn_PA4 != null) {
                btn_PA4.Dispose ();
                btn_PA4 = null;
            }

            if (btn_PA5 != null) {
                btn_PA5.Dispose ();
                btn_PA5 = null;
            }

            if (btn_PA6 != null) {
                btn_PA6.Dispose ();
                btn_PA6 = null;
            }

            if (btn_PA7 != null) {
                btn_PA7.Dispose ();
                btn_PA7 = null;
            }

            if (btn_PA8 != null) {
                btn_PA8.Dispose ();
                btn_PA8 = null;
            }

            if (btn_PA8_left != null) {
                btn_PA8_left.Dispose ();
                btn_PA8_left = null;
            }

            if (btn_PA9 != null) {
                btn_PA9.Dispose ();
                btn_PA9 = null;
            }

            if (bw1_left != null) {
                bw1_left.Dispose ();
                bw1_left = null;
            }

            if (img_bw1 != null) {
                img_bw1.Dispose ();
                img_bw1 = null;
            }

            if (img_BW2 != null) {
                img_BW2.Dispose ();
                img_BW2 = null;
            }

            if (img_BW3 != null) {
                img_BW3.Dispose ();
                img_BW3 = null;
            }

            if (img_BW4 != null) {
                img_BW4.Dispose ();
                img_BW4 = null;
            }

            if (img_PA1 != null) {
                img_PA1.Dispose ();
                img_PA1 = null;
            }

            if (img_PA10 != null) {
                img_PA10.Dispose ();
                img_PA10 = null;
            }

            if (img_PA11 != null) {
                img_PA11.Dispose ();
                img_PA11 = null;
            }

            if (img_PA12 != null) {
                img_PA12.Dispose ();
                img_PA12 = null;
            }

            if (img_PA13 != null) {
                img_PA13.Dispose ();
                img_PA13 = null;
            }

            if (img_PA14 != null) {
                img_PA14.Dispose ();
                img_PA14 = null;
            }

            if (img_PA2 != null) {
                img_PA2.Dispose ();
                img_PA2 = null;
            }

            if (img_PA3 != null) {
                img_PA3.Dispose ();
                img_PA3 = null;
            }

            if (img_PA4 != null) {
                img_PA4.Dispose ();
                img_PA4 = null;
            }

            if (img_PA5 != null) {
                img_PA5.Dispose ();
                img_PA5 = null;
            }

            if (img_PA6 != null) {
                img_PA6.Dispose ();
                img_PA6 = null;
            }

            if (IMG_pa7 != null) {
                IMG_pa7.Dispose ();
                IMG_pa7 = null;
            }

            if (img_PA8 != null) {
                img_PA8.Dispose ();
                img_PA8 = null;
            }

            if (img_Pa8_left != null) {
                img_Pa8_left.Dispose ();
                img_Pa8_left = null;
            }

            if (img_PA9 != null) {
                img_PA9.Dispose ();
                img_PA9 = null;
            }

            if (pa1_left != null) {
                pa1_left.Dispose ();
                pa1_left = null;
            }

            if (scroll_view != null) {
                scroll_view.Dispose ();
                scroll_view = null;
            }
        }
    }
}