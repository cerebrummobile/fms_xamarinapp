using Foundation;
using System;
using UIKit;
using CoreGraphics;
using PRAMotionZoom;

namespace IAO
{
	public partial class InfoViewController : BaseController
	{
		UIImageView imageview;

		public InfoViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			View.BackgroundColor = UIColor.FromRGB(0, 13, 29);

			RightBaritemClickListner();

			LayoutCreation();
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			if (device_type.ToString().Equals("Pad"))
			{
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 23f), ForegroundColor = UIColor.Black };

			}
			else
			{
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 18f), ForegroundColor = UIColor.Black };


			}
		}
		public BaseNavigationController NavController { get; private set; }
		void LayoutCreation()
		{
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			this.NavigationItem.Title = "Information".ToUpper();
			imageview = new UIImageView();
			imageview.Image = UIImage.FromFile("info_IMG");

			if (View.Frame.Width == 736 && View.Frame.Height == 414)
			{
				imageview.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);
			}     

			if (View.Frame.Width == 667 && View.Frame.Height == 375)
			{  
				imageview.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);
			}

			if (View.Frame.Width == 568 && View.Frame.Height == 320)
			{ 
				imageview.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);
			}

			if (View.Frame.Width == 480 && View.Frame.Height == 320)
			{ 
				imageview.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);
			}  

			if (device_type.ToString().Equals("Pad"))
			{
				imageview.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);
			}

			View.AddSubviews(imageview);
		}

		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new CGRect(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{
				HomeScreenControllerIPaid.buttonclicktag = "";
				HomeViewController.ButtonClickTag = "";
						var storyBoard_ = UIStoryboard.FromName("Main", null);
				var initialViewController = storyBoard_.InstantiateViewController("root");
				UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;


				//var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				//var storyBoard = UIStoryboard.FromName("Main", null);
				//if (device_type.ToString().Equals("Pad"))
				//{
				//	var initialViewController = storyBoard.InstantiateViewController("homeScreen_ipad");
				//	var NavigationController = new UINavigationController(initialViewController);
				//	NavigationController.NavigationBar.BackgroundColor = UIColor.White;
				//	UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
				//	HomeScreenControllerIPaid.buttonclicktag = string.Empty;
				//}
				//else
				//{ 
					//var storyBoard_ = UIStoryboard.FromName("Main", null);
			//var initialViewController = storyBoard_.InstantiateViewController("HomeController");
			//UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;

				

					//var content = Storyboard.InstantiateViewController("HomeController") as HomeViewController;
					//NavController.PushViewController(content, false);
					//var initialViewController = storyBoard.InstantiateViewController("HomeController");
					//var NavigationController = new UINavigationController(initialViewController);
					//NavigationController.NavigationBar.BackgroundColor = UIColor.White;
					//UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
					//HomeViewController.ButtonClickTag = string.Empty;
				//}
			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}
	}
}