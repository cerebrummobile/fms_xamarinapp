using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace IAO
{
	public partial class HelpViewController : BaseController
    {
        UIButton btn_Image1, btn_Image2;

		public HelpViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//	View.BackgroundColor = UIColor.FromRGB(0, 13, 29);
			View.BackgroundColor = UIColor.White;

			LayoutCreation();
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { Font = UIFont.FromName("OpenSans-Light", 18f), ForegroundColor = UIColor.Black };
		// call this method for home button at the right top side
			RightBaritemClickListner();
		}

		void LayoutCreation()
		{
			var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
			this.NavigationItem.Title = "Help".ToUpper();
			//btn_Image1 = new UIButton();
			//btn_Image1.SetBackgroundImage(UIImage.FromFile("help_IMG1"), UIControlState.Normal);
			//btn_Image1.BackgroundColor = UIColor.Red;
			//btn_Image1.TouchUpInside += (sender, e) =>
			//{
			//	var helpimage = Storyboard.InstantiateViewController("helpimage") as HelpImageViewScreen;
			//	helpimage.firstImage = "Image1";
			//	NavigationController.PushViewController(helpimage, true);
			//};

			//btn_Image2 = new UIButton();
			//btn_Image2.SetBackgroundImage(UIImage.FromFile("help_IMG2"), UIControlState.Normal);
			//btn_Image2.BackgroundColor = UIColor.Red;
			//btn_Image2.TouchUpInside += (sender, e) =>
			//{
			//	var helpimage = Storyboard.InstantiateViewController("helpimage") as HelpImageViewScreen;
			//	helpimage.secondImage = "Image2";
			//	NavigationController.PushViewController(helpimage, true);
			//};

			if (View.Frame.Width == 736 && View.Frame.Height == 414)
			{//6S PLUS
				//btn_Image1.Frame = new CGRect(View.Frame.X + 60, 130, 280, 180);
				//btn_Image2.Frame = new CGRect(btn_Image1.Frame.Width + btn_Image1.Frame.X + 50, 130, 280, 180);
			}

			if (View.Frame.Width == 667 && View.Frame.Height == 375)
			{//6S
				//btn_Image1.Frame = new CGRect(View.Frame.X + 60, 120, 250, 150);
				//btn_Image2.Frame = new CGRect(btn_Image1.Frame.Width + btn_Image1.Frame.X + 50, 120, 250, 150);
			}

			if (View.Frame.Width == 568 && View.Frame.Height == 320)
			{//5S
				//btn_Image1.Frame = new CGRect(View.Frame.X + 60, 100, 200, 120);
				//btn_Image2.Frame = new CGRect(btn_Image1.Frame.Width + btn_Image1.Frame.X + 50, 100, 200, 120);
			}

			if (View.Frame.Width == 480 && View.Frame.Height == 320)
			{//4s same as 5s
				//btn_Image1.Frame = new CGRect(View.Frame.X + 60, 100, 180, 120);
				//btn_Image2.Frame = new CGRect(btn_Image1.Frame.Width + btn_Image1.Frame.X + 50, 100, 180, 120);
			}

			if (device_type.ToString().Equals("Pad"))
			{
				//btn_Image1.Frame = new CGRect(View.Frame.X + 80, 250, 400, 300);
				//btn_Image2.Frame = new CGRect(btn_Image1.Frame.Width + btn_Image1.Frame.X + 50, 250, 400, 300);
			}

			//View.AddSubviews(btn_Image1, btn_Image2);
		}

		protected void RightBaritemClickListner()
		{
			var image = UIImage.FromBundle("app_logo");
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetBackgroundImage(image, UIControlState.Normal);
			button.Frame = new CGRect(0, 0, 30, 30);
			button.TouchUpInside += delegate
			{   HomeScreenControllerIPaid.buttonclicktag = "";
				HomeViewController.ButtonClickTag = "";
					var storyBoard_ = UIStoryboard.FromName("Main", null);
				var initialViewController = storyBoard_.InstantiateViewController("root");
				UIApplication.SharedApplication.KeyWindow.RootViewController = initialViewController;
				//var device_type = UIDevice.CurrentDevice.UserInterfaceIdiom;
				//var storyBoard = UIStoryboard.FromName("Main", null);
				//if (device_type.ToString().Equals("Pad"))
				//{
				//	var initialViewController = storyBoard.InstantiateViewController("homeScreen_ipad");
				//	var NavigationController = new UINavigationController(initialViewController);
				//	NavigationController.NavigationBar.BackgroundColor = UIColor.White;
				//	UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
				//	HomeScreenControllerIPaid.buttonclicktag = string.Empty;
				//}
				//else
				//{
				//	var initialViewController = storyBoard.InstantiateViewController("HomeController");
				//	var NavigationController = new UINavigationController(initialViewController);
				//	NavigationController.NavigationBar.BackgroundColor = UIColor.White;
				//	UIApplication.SharedApplication.KeyWindow.RootViewController = NavigationController;
				//	HomeViewController.ButtonClickTag = string.Empty;
				//}



			};
			NavigationItem.RightBarButtonItem = new UIBarButtonItem(button);
		}
	}
}